<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Index extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->model('model');
		$this->load->library('session');
	}
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function check_index()
	{
		if ($this->session->userdata('username')) {
			redirect('index/admin');
		}
	}
	public function check_admin()
	{
		if ($this->session->userdata('username')) {
		}else{
			redirect('index');
		}
	}
	function logout()
	{
		$this->session->sess_destroy();
		redirect('index');
	}
	public function index()
	{
		$this->check_index();
		$data['title']='Login';
		$this->load->view('index',$data);
	}
	public function tambah_pengirim()
	{
		$data = array(
			'nama_pengirim' => $_POST['nama_pengirim'],
			'no_telp_pengirim' => $_POST['telp_pengirim'],
			'alamat_pengirim' => $_POST['alamat_pengirim'],
			);
		$this->db->insert('pengirim',$data);
		echo "<script>alert('berhasil ditambahkan.')</script>";
		echo "<script>window.history.back();</script>";
	}
	public function admin()
	{
		$this->check_admin();
		$data['title']='Administrator';
		$this->load->view('index_admin',$data);
	}
	public function out($id='')
	{
		$data['title'] = 'Print';
		if (!empty($id)) {
			$data['data']=$this->db->get_where('penerima',array('id_penerima'=>$id));
		}else{
			$id=1;
			$data['data']=$this->db->get_where('penerima',array('id_penerima'=>$id));
		}
		$this->load->view('print',$data);
	}
	public function out_xlx($id='')
	{
		$data['title'] = 'Print';
		if (!empty($id)) {
			$data['data']=$this->db->get_where('penerima',array('id_penerima'=>$id));
		}else{
			$id=1;
			$data['data']=$this->db->get_where('penerima',array('id_penerima'=>$id));
		}
		$this->load->view('convert_xlx',$data);
	}
	public function laporan()
	{
		if (empty($_POST['tgl1'])) {
			$tgl1 = date('Y-m-d');
		}else{
			$tgl1=$_POST['tgl1'];
		}
		if (empty($_POST['tgl2'])) {
			$tgl2 = date('Y-m-d');
		}else{
			$tgl2=$_POST['tgl2'];
		}
		$data['title']='Laporan/History';
		$data['laporan'] = $this->db->query("SELECT * FROM penerima WHERE date(tanggal_dibuat) BETWEEN '".date('Y-m-d',strtotime($tgl1))."' AND '".date('Y-m-d',strtotime($tgl2))."' ORDER BY id_penerima DESC");
		// print_r($data['laporan']->result());
		// echo date('Y-m-d',strtotime($tgl1));
		// die();
		$this->load->view('laporan',$data);
	}
	public function printlaporan($tgl=FALSE,$tgl2=FALSE)
	{
		$data['title']='Laporan/History';
		if ($tgl) {
			$this->db->where("date(tanggal_dibuat) BETWEEN '$tgl' AND '$tgl2'");
		}
		$data['laporan'] = $this->db->order_by('id_penerima','ASC')->get('penerima');
		$tgl_n = explode('-',$tgl);
		$data['tgl']=$tgl_n[2]."-".$tgl_n[1]."-".$tgl_n[0];
		$data['tgl2']=$tgl2;
		$this->load->view('printlaporan',$data);
	}
	public function daftar_pengirim()
	{
		if ($this->input->post('nama_pengirim')) {
			$upd = elements(array('nama_pengirim','no_telp_pengirim','alamat_pengirim'), $this->input->post(), '-');
			$this->db->where('id_pengirim', $this->input->post('id_pengirim'));
			$this->db->update('pengirim', $upd);
			exit();
		}
		$data['title']='Daftar Pengirim';
		$data['daftar'] = $this->db->order_by('id_pengirim','DESC')->get('pengirim');
		$this->load->view('daftar_pengirim',$data);
	}
	public function delete_kasir($id='')
	{
		if ($id) {
			$this->db->where('id_kasir',$id);
			$this->db->delete('kasir');
			echo "<script>alert('Berhasil dihapus')</script>";
			echo "<script>window.history.back()</script>";
		}else{
			echo "<script>alert('Gagal Dihapus')</script>";
			echo "<script>window.history.back()</script>";
		}
	}
	public function hapus_pengirim($id='')
	{
		$this->db->where('id_pengirim',$id);
		$this->db->delete('pengirim');
		echo "<script>alert('Berhasil dihapus')</script>";
		echo "<script>window.history.back()</script>";
	}
	public function hapus_data($id='')
	{
		$pass = $this->db->query("SELECT secret_password FROM password ")->row();
		if ($_POST['password_delete']==$pass->secret_password) {
			$this->db->where('id_penerima',$id);
			$this->db->delete('penerima');
			$this->session->set_flashdata('msg', '<div class="alert alert-success">Berhasil.</div><hr>');
			redirect('index/laporan');
		}else{
			$this->session->set_flashdata('msg', '<div class="alert alert-danger">Kode salah, Silahkan Ulangi kembali.</div><hr>');
			redirect('index/laporan');
		}
	}
	function delete_multiple()
	{
		if ($this->input->post()) {
			$sql="delete from penerima where tanggal_dibuat >= '".date('Y-m-d',strtotime($this->input->post('tgl_delete1')))."' AND tanggal_dibuat <= '".date('Y-m-d',strtotime($this->input->post('tgl_delete2')))."'";
			if ($this->db->query($sql)) {
				echo "<script>alert('Berhasil dihapus')</script>";
				echo "<script>window.history.back()</script>";
			}
		}else{
			redirect('index/laporan');
		}
	}
	public function form()
	{
		if ($this->input->post()) {
			$this->saved();
		}
		$this->check_admin();
		$data['title']='Add Report';
		$data['provinsi'] = $this->db->query("SELECT * FROM provinces");
		$data['option_pengirim']=$this->db->get('pengirim')->result();
		$data['option_kasir']=$this->db->get('kasir')->result();
		$this->load->view('form_report',$data);
	}
	public function saved()
	{
		$insert = elements(array('type_choice_kg','type_choice_vol','tarif_per_kg','subtotal','tarif_per_vol','chargeable','nama_pengirim', 'dp_kredit', 'rincian_per_kg', 'alamat_pengirim', 'no_telp_pengirim','vol', 'nama_penerima', 'alamat_penerima', 'no_telp_penerima', 'asal', 'tujuan', 'jenis_pengiriman', 'service', 'isi_barang', 'no_smu', 'nama_pesawat', 'kg', 'koli', 'kode_airline', 'cara_pembayaran', 'karantina','pick_up','packing','handling','lain_lain','adm_smu','service_charge','nama_personel'), $this->input->post());
		$insert['no_rek'] = '3008 01 00 23 02 504';
		$insert['bank'] = 'BRI';
		$insert['tanggal_dibuat'] = date('Y-m-d H:i:s');
		$insert['tanggal_berangkat'] = date('Y-m-d',strtotime($_POST['tanggal_berangkat']));
		if ($_POST['type_choice_kg'] == '0') {
			if($_POST['rincian_per_kg']){
			$array=$_POST['rincian_per_kg'];
			$rincian_per_kg = array();
				foreach($array as $value){
					array_push($rincian_per_kg, $value);
				}
			}
			$insert['rincian_per_kg'] = implode(',',$rincian_per_kg);
		}else{
			$insert['rincian_per_kg'] = '';
			// $insert['jumlah_kali_kg'] = $_POST['jumlah_kali_kg'];
			$insert['value_nilai_kg'] = $_POST['value_nilai_kg'];
		}

		if ($_POST['type_choice_vol'] == '0') {
			if($_POST['rincian_per_vol']){
			$array_vol=$_POST['rincian_per_vol'];
			$rincian_per_vol = array();
				foreach($array_vol as $value_vol){
					array_push($rincian_per_vol, $value_vol);
				}
			}
			$insert['rincian_per_vol'] = implode(',',$rincian_per_vol);
		}else{
			$insert['rincian_per_vol'] = '';
			// $insert['jumlah_kali_vol'] = $_POST['jumlah_kali_vol'];
			$insert['value_nilai_vol'] = $_POST['value_nilai_vol'];
		}
		// $insert['rincian_per_vol'] = $_POST['rincian_per_vol1'].",".$_POST['rincian_per_vol2'].",".$_POST['rincian_per_vol3'].",".$_POST['rincian_per_vol4'].",".$_POST['rincian_per_vol5'].",".$_POST['rincian_per_vol6'].",".$_POST['rincian_per_vol7'].",".$_POST['rincian_per_vol8'].",".$_POST['rincian_per_vol9'].",".$_POST['rincian_per_vol10'];
		// $insert['rincian_per_kg'] = $_POST['rincian_per_kg1'].",".$_POST['rincian_per_kg2'].",".$_POST['rincian_per_kg3'].",".$_POST['rincian_per_kg4'].",".$_POST['rincian_per_kg5'].",".$_POST['rincian_per_kg6'].",".$_POST['rincian_per_kg7'].",".$_POST['rincian_per_kg8'].",".$_POST['rincian_per_kg9'].",".$_POST['rincian_per_kg10'];

		$this->db->insert('penerima', $insert);
		$id=$this->db->insert_id();
		redirect(base_url('index/out/'.$id));
	}
	public function login()
	{
		$cek=$this->model->login($this->input->post('username'),md5($this->input->post('password')));
		if ($cek) {
			$data = array('username' =>$cek->sys_username_account);
			$this->session->set_userdata($data);
			echo "berhasil";
		}else{
			echo "gagal";
		}
	}

	public function edit_data_pass($id)
	{
		if (!empty($id)) {
			$pass = $this->db->query("SELECT secret_password FROM password ")->row();
			if ($_POST['password_delete']==$pass->secret_password) {
				redirect('index/edit_data/'.$id);
			}else{
				$this->session->set_flashdata('msg', '<div class="alert alert-danger">Kode salah, Silahkan Ulangi kembali.</div><hr>');
				redirect('index/laporan');
			}
		}
	}

	public function edit_data($id)
	{
		$pass = $this->db->query("SELECT secret_password FROM password ")->row();
		// print_r(strtolower($_POST['password_edit']));
		// print_r(strtolower($pass->secret_password));
		// die();
		if (strtolower(@$_POST['password_edit'])==strtolower($pass->secret_password) OR @$_SESSION['password_edit']) {
			$_SESSION['password_edit'] = 'password_edit';
			if ($this->input->post('nama_personel') && $this->input->post('nama_pengirim')) {
				if ($_POST['nama_personel']) {
					$update = elements(array('type_choice_vol','type_choice_kg','tarif_per_kg','subtotal','tarif_per_vol','chargeable','nama_pengirim', 'dp_kredit', 'rincian_per_kg','vol', 'alamat_pengirim', 'no_telp_pengirim', 'nama_penerima', 'alamat_penerima', 'no_telp_penerima', 'asal', 'tujuan', 'jenis_pengiriman', 'service', 'isi_barang', 'no_smu', 'nama_pesawat', 'kg', 'koli', 'kode_airline', 'cara_pembayaran', 'service_charge','karantina','pick_up','packing','handling','lain_lain','adm_smu','service_charge','nama_personel'), $this->input->post());
				}else{
					$update = elements(array('type_choice_vol','type_choice_kg','tarif_per_kg','subtotal','tarif_per_vol','chargeable','nama_pengirim', 'dp_kredit', 'rincian_per_kg','vol', 'alamat_pengirim', 'no_telp_pengirim', 'nama_penerima', 'alamat_penerima', 'no_telp_penerima', 'asal', 'tujuan', 'jenis_pengiriman', 'service', 'isi_barang', 'no_smu', 'nama_pesawat', 'kg', 'koli', 'kode_airline', 'cara_pembayaran', 'service_charge','karantina','pick_up','packing','handling','lain_lain','adm_smu','service_charge'), $this->input->post());
				}
				$update['tanggal_berangkat'] = date('Y-m-d',strtotime($_POST['tanggal_berangkat']));
				$update['no_rek'] = '3008 01 00 23 02 504';
				$update['bank'] = 'BRI';
				if ($_POST['type_choice_kg'] == '0') {
					if($_POST['rincian_per_kg']){
					$array=$_POST['rincian_per_kg'];
					$rincian_per_kg = array();
						foreach($array as $value){
							array_push($rincian_per_kg, $value);
						}
					}
					$update['rincian_per_kg'] = implode(',',$rincian_per_kg);
				}else{
					$update['rincian_per_kg'] = '';
					// $update['jumlah_kali_kg'] = $_POST['jumlah_kali_kg'];
					$update['value_nilai_kg'] = $_POST['value_nilai_kg'];
				}

				if ($_POST['type_choice_vol'] == '0') {
					if($_POST['rincian_per_vol']){
					$array_vol=$_POST['rincian_per_vol'];
					$rincian_per_vol = array();
						foreach($array_vol as $value_vol){
							array_push($rincian_per_vol, $value_vol);
						}
					}
					$update['rincian_per_vol'] = implode(',',$rincian_per_vol);
				}else{
					$update['rincian_per_vol'] = '';
					// $update['jumlah_kali_vol'] = $_POST['jumlah_kali_vol'];
					$update['value_nilai_vol'] = $_POST['value_nilai_vol'];
				}
				// $update['rincian_per_kg'] = $_POST['rincian_per_kg1'].",".$_POST['rincian_per_kg2'].",".$_POST['rincian_per_kg3'].",".$_POST['rincian_per_kg4'].",".$_POST['rincian_per_kg5'].",".$_POST['rincian_per_kg6'].",".$_POST['rincian_per_kg7'].",".$_POST['rincian_per_kg8'].",".$_POST['rincian_per_kg9'].",".$_POST['rincian_per_kg10'];
				// $update['rincian_per_vol'] = $_POST['rincian_per_vol1'].",".$_POST['rincian_per_vol2'].",".$_POST['rincian_per_vol3'].",".$_POST['rincian_per_vol4'].",".$_POST['rincian_per_vol5'].",".$_POST['rincian_per_vol6'].",".$_POST['rincian_per_vol7'].",".$_POST['rincian_per_vol8'].",".$_POST['rincian_per_vol9'].",".$_POST['rincian_per_vol10'];
				$this->db->where('id_penerima', $id);
				$this->db->update('penerima', $update);
				$this->session->set_flashdata('msg', '<div class="alert alert-success">Berhasil</div><hr>');
				redirect(current_url());
			}
			@session_destroy('password_edit');
			$this->check_admin();

			$this->db->where('id_penerima', $id);
			$x = $this->db->get('penerima');
			if ($x->num_rows() == 0) {
				redirect('index/laporan');
			}

			$data['data'] = $x->row();

			$data['title']='Edit Report';
			$data['option_kasir']=$this->db->get('kasir')->result();
			$data['option_pengirim']=$this->db->get('pengirim')->result();
			$this->load->view('edit_data',$data);
		}else{
			$this->session->set_flashdata('msg', '<div class="alert alert-danger">Kode salah, Silahkan Ulangi kembali.</div><hr>');
			redirect('index/laporan');
		}
	}
	public function rahasia($type='')
	{
		if ($type=='') {
			if (!empty($_POST['new_password'])) {
				$data['title']='Rahasia - Ganti Password';
				$update['secret_password'] = $_POST['new_password'];
				$this->db->where('id_password', 1);
				$this->db->update('password', $update);
				echo "<script>alert('Berhasil diupdate')</script>";
				echo "<script>window.history.back()</script>";
			}else{
				$data['password']=$this->db->get_where('password',array('id_password'=>1))->row();
				$this->load->view('rahasia',$data);
			}
		}else if ($type == 'kasir') {
			$data['title']='Kasir - Data Personel';
			$data['kasir']=$this->db->get('kasir');
			$this->load->view('kasir',$data);
		}
	}
	public function tambah_kasir()
	{
		if ($_POST) {
			$data = array(
				'nama_kasir' => $_POST['nama_kasir'],
				'tgl_dibuat' => date('Y-m-d H:i:s'),
				);
			if ($this->db->insert('kasir',$data)) {
				echo "<script>alert('Berhasil dibuat')</script>";
				echo "<script>window.history.back()</script>";
			}else{
				echo "<script>alert('Gagal dibuat, Silahkan Ulangi Kembali')</script>";
				echo "<script>window.history.back()</script>";
			}
		}else{
			echo "<script>alert('Gagal dibuat, Silahkan Ulangi Kembali')</script>";
			echo "<script>window.history.back()</script>";
		}
	}
}
