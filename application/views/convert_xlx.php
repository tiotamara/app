<?php
$file="data.xls";
header("Content-type: application/vnd.ms-excel");
header("Content-Disposition: attachment; filename=$file");
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title><?php echo $title; ?></title>
	<link rel="stylesheet" href="<?php echo base_url('dist/css/bootstrap.min.css'); ?>">
	<!-- ================= -->
	<link rel="stylesheet" href="<?php echo base_url('dist/css/custom.css'); ?>">
	<!-- <link rel='stylesheet prefetch' href='http://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900|RobotoDraft:400,100,300,500,700,900'> -->
	<link rel='stylesheet prefetch' href='<?php echo base_url('dist/css/font-awesome.min.css'); ?>'>
	<script src='<?php echo base_url('dist/js/jquery.min.js'); ?>'></script>
	<!-- ===================== -->
	<style>
		td {    width: 60px;}
		body{
			font-size: 10px;
		}
	</style>
</head>
<body>

	<?php 
	if ($data->num_rows() > 0) {
		foreach ($data->result() as $data_print) { ?>
			<div class="container">
				<table style="width:100%;" border="1" class="table-hover table-bordered">
					<tbody>

						<tr>
							<td width="20"></td>
							<td width="20"></td>
							<td width="20"></td>
							<td width="20"></td>
							<td width="20"></td>
							<td width="20"></td>
							<td width="20"></td>
							<td width="20"></td>
							<td width="20"></td>
							<td width="20"></td>
							<td width="20"></td>
							<td width="20"></td>
							<td width="20"></td>
							<td width="20"></td>
							<td width="20"></td>
							<td width="20"></td>
							<td width="20"></td>
							<td width="20"></td>
							<td width="20"></td>
							<td width="20"></td>
							<td width="20"></td>
							<td width="20"></td>
						</tr>
						<tr>
							<td style="vertical-align:middle;" colspan="2" rowspan="3"> <img class="img-responsive" width="100" src="<?php echo base_url('image/logo.jpg'); ?>" alt="Logo"> </td>							<td style="vertical-align:middle;" colspan="14"><h3 style="margin:0;"><font face="Baskerville Old Face">PT. TRANS SARANA JAYA</font></h3></td>
							<td style="text-align:center;" colspan="4"><h5 style="margin:0;">INVOICE</h5></td>
						</tr>
						<!-- ===================================== -->
						<tr>
							<td colspan="14"><?php echo ucwords($data_print->nama_personel) ?><br>JL. NGESIKGONDO NO.55, KOTA GEDE (DEPAN RS KIA Permata Bunda) YOGYAKARTA</td>
							<td colspan="1">No:</td>
							<td style="text-align:center;" colspan="3"><strong><?php echo $data_print->id_penerima; ?></strong></td>
						</tr>
						<!-- ===================================== -->
						<tr>
							<td colspan="14">No. CS : 0823 2741 6666 / 0857 2844 4427 <span class="pull-right">No. Fax : 0274 2840296</span>	</td>
							<td colspan="4"></td>
						</tr>
						<!-- ===================================== -->
						<tr>
							<td colspan="9"><h5 style="margin:0;">PENGIRIM</h5></td>
							<td colspan="9"><h5 style="margin:0;">PENERIMA</h5></td>
							<td style="text-align:center;" colspan="2">ASAL</td>
							<td style="text-align:center;" colspan="2"><?php echo ucwords($data_print->asal) ?></td>
						</tr>
						<!-- ===================================== -->
						<tr>
							<td colspan="2">Nama</td>
							<td colspan="7">: <?php echo ucwords($data_print->nama_pengirim) ?></td>
							<td colspan="2">Nama</td>
							<td colspan="7">: <?php echo ucwords($data_print->nama_penerima) ?></td>
							<td style="text-align:center;" colspan="2">TUJUAN</td>
							<td style="text-align:center;" colspan="2"><b><?php echo strtoupper($data_print->kode_airline) ?></b></td>
						</tr>
						<!-- ===================================== -->
						<tr>
							<td colspan="2" style="border-bottom:0;">Alamat</td>
							<td colspan="7" rowspan="3">: <?php echo ucwords($data_print->alamat_pengirim) ?></td>
							<td colspan="2" style="border-bottom:0;">Alamat</td>
							<td colspan="7" rowspan="3">: <?php echo ucwords($data_print->alamat_penerima) ?></td>
							<td style="text-align:center;" colspan="4"><b><?php echo strtoupper($data_print->tujuan) ?></b></td>
						</tr>
						<!-- ===================================== -->
						<tr>
							<td colspan="2" rowspan="2" style="border-top:0;"></td>
							<td colspan="2" rowspan="2" style="border-top:0;"></td>
							<td style="text-align:center;" colspan="4">Jenis Pengiriman</td>
						</tr>
						<!-- ===================================== -->
						<tr>
							<td style="text-align:center;" colspan="2" rowspan="2"><?php echo strtoupper($data_print->jenis_pengiriman); ?></td>
							<td style="text-align:center;" colspan="2" rowspan="2"><?php echo strtoupper($data_print->service); ?></td>
						</tr>
						<!-- ===================================== -->
						<tr>
							<td colspan="2">No. Telp</td>
							<td colspan="7">: <?php echo ucwords($data_print->no_telp_pengirim) ?></td>
							<td colspan="2">No. Telp</td>
							<td colspan="7">: <?php echo ucwords($data_print->no_telp_penerima) ?></td>
						</tr>
						<!-- ===================================== -->
						<tr>
							<td style="text-align:center;" colspan="4"><b>Isi Barang</b></td>
							<td style="text-align:center;" colspan="4"><b>No. SMU</b></td>
							<td style="text-align:center;" colspan="2"><b>A/C</b></td>
							<td style="text-align:center;" colspan="3"><b>Tgl Berangkat</b></td>
							<td style="text-align:center;" colspan="5"><b>Rincian Pengiriman</b></td>
							<td style="text-align:center;" colspan="4"><b>Jumlah Rp</b></td>
						</tr>
						<!-- ===================================== -->
						<tr>
							<td style="text-align:center;" colspan="4" rowspan="2" contenteditable><?php echo $data_print->isi_barang ?></td>
							<td style="text-align:center;" colspan="4"><?php echo ucwords($data_print->nama_pesawat); ?></td>
							<td style="text-align:center;" colspan="2" rowspan="2">
								<?php 
								if ($data_print->nama_pesawat=='nam_air') {
									echo "IN";
								}elseif ($data_print->nama_pesawat=='garuda') {
									echo "GA";
								}elseif ($data_print->nama_pesawat=='lion_air') {
									echo "JT";
								}elseif ($data_print->nama_pesawat=='sriwijaya') {
									echo "SJ";
								}elseif ($data_print->nama_pesawat=='citi_link') {
									echo "QG";
								}elseif ($data_print->nama_pesawat=='air_asia') {
									echo "QZ";
								}elseif ($data_print->nama_pesawat=='wing_air') {
									echo "IW";
								}elseif ($data_print->nama_pesawat=='batik_air') {
									echo "ID";
								}elseif ($data_print->nama_pesawat=='truk') {
									echo "TRK";
								}elseif ($data_print->nama_pesawat=='kapal') {
									echo "KPL";
								}
							 ?>
							</td>
							<td style="text-align:center;" colspan="3" rowspan="2"><?php echo date('d F Y', strtotime($data_print->tanggal_berangkat)); ?></td>
							<td colspan="5">tarif Rp. Per Kg/Vol</td>
							<td colspan="4"></td>
						</tr>
						<!-- ===================================== -->
						<tr>
							<td style="text-align:center;" colspan="4"><?php echo $data_print->no_smu; ?></td>
							<td colspan="2"><?php 
							if ($data_print->kg > $data_print->vol) {
								echo "Rp. ".rupiah($data_print->tarif_per_kg);
							}else{
								echo "Rp. ".rupiah($data_print->tarif_per_vol);
							}
							 ?></td>
							<td>x</td>
							<td colspan="2"><?php 
							echo $data_print->chargeable;
							 ?></td>
							<td colspan="4"><?php 
							if ($data_print->kg > $data_print->vol) {
								echo rupiah($data_print->tarif_per_kg*$data_print->chargeable);
							}else{
								echo rupiah($data_print->tarif_per_vol*$data_print->chargeable);
							}
							 ?></td>
						</tr>
						<!-- ===================================== -->
						<tr>
							<td style="text-align:center;" colspan="2"><b>Koli</b></td>
							<td style="text-align:center;" colspan="2"><b>Total Kg/Vol</b></td>
							<td style="text-align:center;" colspan="9"><b>Rincian koli per kg</b></td>
							<td colspan="2"></td>
							<td></td>
							<td colspan="2"></td>
							<td colspan="4"></td>
						</tr>
						<!-- ===================================== -->
						<tr>
							<td style="text-align:center;" colspan="2" rowspan="2"><?php echo $data_print->koli; ?></td>
							<td style="text-align:center;" colspan="2"><?php echo $data_print->kg; ?></td>
							<td>Kg</td>
							<td style="text-align:center;" colspan="8"><?php 
							if ($data_print->rincian_per_kg==0) {
								echo "0";
							}else{
								$vol = explode(',', $data_print->rincian_per_kg);
								for ($i=0; $i < count($vol) ; $i++) { 
									if ($vol[$i]==0) {
										
									}else{
										if ($vol[$i]!=count($vol)-1) {
											echo $vol[$i].",";
										}else{
											echo $vol[$i];
										}
									}
								}
							}
							 ?></td>
							<td colspan="5" rowspan="4">Biaya-biaya lain: <br> - Karantina <br> - Ppn <br> - Packing : <br><br> - Gudang <br> - Lain-lain <br> - Adm. SMU</td>
							<td colspan="4" rowspan="4"><br><?php echo rupiah($data_print->karantina); ?><br><?php echo rupiah($data_print->pick_up); ?><br><?php echo rupiah($data_print->packing); ?><br><br><?php echo rupiah($data_print->handling); ?><br><?php echo rupiah($data_print->lain_lain); ?><br><?php echo rupiah($data_print->adm_smu); ?></td>
						</tr>
						<!-- ===================================== -->
						<tr>
							<td style="text-align:center;" colspan="2"><?php echo $data_print->vol; ?></td>
							<td>Vol</td>
							<td style="text-align:center;" colspan="8"><?php 
							if ($data_print->rincian_per_vol==0) {
								echo "0";
							}else{
								$vol = explode(',', $data_print->rincian_per_vol);
								for ($i=0; $i < count($vol) ; $i++) { 
									if ($vol[$i]==0) {
										
									}else{
										if ($vol[$i]!=count($vol)-1) {
											echo $vol[$i].",";
										}else{
											echo $vol[$i];
										}
									}
								}
							}
							 ?></td>
						</tr>
						<!-- ===================================== -->
						<tr>
							<td style="text-align:center;" colspan="13">Isi barang dinyatakan sesuai dan benar menurut pengakuan Pengirim. tidak berupa cairan dan tidak berbahaya serta aman untuk semua jenis Pengangkutan. dengan ini, saya menyatakan dan menyutujui bahwa segalam bentuk risiko menjadi tanggung jawab saya serta saya menyetujui syarat-syarat dan ketentuan Pengiriman PT. TRANS SARANA JAYA dan Pihak Pengangkut/Airline.</td>
						</tr>
						<!-- ===================================== -->
						<tr>
							<td colspan="7">Cara Pembayaran:</td>
							<td style="text-align:center;" colspan="6">Persetujuan Pengirim</td>
						</tr>
						<!-- ===================================== -->
						<tr>
							<td colspan="3"><input type="checkbox" disabled="disabled" <?php if($data_print->cara_pembayaran=='cash'){ echo "checked"; } ?>> CASH</td>
							<td colspan="4"><input type="checkbox" disabled="disabled" <?php if($data_print->cara_pembayaran=='kredit'){ echo "checked"; } ?>> CREDIT</td>
							<td colspan="6"></td>
							<td colspan="5"><h4 style="margin:0;text-align:center;"><strong>T O T A L</strong></h4></td>
							<?php $biayalain=$data_print->karantina+$data_print->pick_up+$data_print->packing+$data_print->handling+$data_print->lain_lain+$data_print->adm_smu; ?>
							<td colspan="4"><p style="margin:0;text-align:center;font-size:14px;"><strong><?php 
							if ($data_print->kg > $data_print->vol) {
								echo "Rp. ".rupiah(($data_print->tarif_per_kg*$data_print->chargeable)+$biayalain); 
							}else{
								echo "Rp. ".rupiah(($data_print->tarif_per_vol*$data_print->chargeable)+$biayalain); 
							}
							 ?></strong></p></td>
						</tr>
					</tbody>
				</table>

				
			</div>
		<?php }
	}else{
		echo "<center><h3>Data Tidak Ditemukan.</h3></center>";
	}
	 ?>
	
	<script src="<?php echo base_url('dist/js/chosen.jquery.js'); ?>" type="text/javascript"></script>
	<script src="<?php echo base_url('dist/js/prism.js'); ?>" type="text/javascript" charset="utf-8"></script>
</body>
</html>
