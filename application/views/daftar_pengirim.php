<!DOCTYPE html>
<html lang="en">
<head>
	<title><?php echo $title; ?></title>
	<link rel="stylesheet" href="<?php echo base_url('dist/css/bootstrap.min.css'); ?>">
	<!-- ================= -->
	<link rel="stylesheet" href="<?php echo base_url('dist/css/custom.css'); ?>">
	<!-- <link rel='stylesheet prefetch' href='http://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900|RobotoDraft:400,100,300,500,700,900'> -->
	<link rel='stylesheet prefetch' href='<?php echo base_url('dist/css/font-awesome.min.css'); ?>'>
	<link rel='stylesheet prefetch' href='<?php echo base_url('dist/DataTables/datatables.css'); ?>'>
	<script src='<?php echo base_url('dist/js/jquery.min.js'); ?>'></script>
	<script src='<?php echo base_url('dist/js/bootstrap.min.js'); ?>'></script>
	<script src='<?php echo base_url('dist/DataTables/datatables.min.js'); ?>'></script>
	<!-- ===================== -->
	<style>
	<style>
	.border-nol{
		border-radius:0px;
	}
	.logo:hover{
		box-shadow: 0px 5px 30px -15px #000;
	}
	</style>
	</style>
</head>
<body>
	<div class="container">
		<div class="row">
			<a href="<?php echo base_url('index/admin'); ?>"><center><img src="<?php echo base_url('image/logo.jpg') ?>" class="img-responsive" style="box-shadow: 0px 5px 30px -15px #000;"></center></a>
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<div class="box">
					<div class="box-icon">
						<span class="fa fa-4x fa-users"></span>
					</div>
					<div class="info">
						<!-- Modal -->
						<div class="modal fade" id="tbp">
							<div class="modal-dialog">
								<div class="modal-content border-nol">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
										<h5 class="modal-title pull-left"><span class="glyphicon glyphicon-user"></span> Tambah Pengirim</h5>
									</div>
									<form method="POST" action="<?php echo base_url('index/tambah_pengirim'); ?>">
										<div class="modal-body">
											<div class="form-group">
												<!-- <label>Nama</label> -->
												<input type="text" class="border-nol form-control input-sm" name="nama_pengirim" placeholder="Nama Pengirim" required>
											</div>
											<div class="form-group">
												<!-- <label>No. Telpon</label> -->
												<input type="number" class="border-nol form-control input-sm" name="telp_pengirim" placeholder="No. Telp" required>
											</div>
											<div class="form-group">
												<!-- <label>Alamat</label> -->
												<textarea class="border-nol form-control" name="alamat_pengirim" placeholder="Alamat Pengirim" required></textarea>
											</div>
										</div>
										<div class="modal-footer">
											<button type="submit" class="btn btn-primary border-nol border-nol"><span class="glyphicon glyphicon-send"></span> Tambah</button>
										</div>
									</form>
								</div><!-- /.modal-content -->
							</div><!-- /.modal-dialog -->
						</div><!-- /.modal -->
						<!-- ./Modal -->
						<h4 class="text-center">Data Pengirim</h4>
						<p>Daftar Data Pengirim</p><div class="clearfix"></div>
						<span class="pull-right"><button type="button" class="btn btn-success pull-right" id="btn-tbp"><span class="glyphicon glyphicon-plus"></span> Tambah</button><button style="margin-right:2px;" class="btn btn-info" data-placement="top" data-toggle="popover" data-container="body" data-trigger="focus" data-content="Untuk mengedit tinggal klik pada nama/no telp/alamat kemudian klik update" title="Info Edit"><span class="glyphicon glyphicon-info-sign"></span></button></span>
						<div class="clearfix"></div><br>
						<div class="table-responsive">
								<table class="table table-hover" style="text-align: left;" id="data-table">
									<thead>
										<tr>
											<th>No</th>
											<th><span class="glyphicon glyphicon-user"></span> Nama Pengirim</th>
											<th><span class="glyphicon glyphicon-phone-alt"></span> No. Telp</th>
											<th><span class="glyphicon glyphicon-map-marker"></span> Alamat</th>
											<th>Edit</th>
											<!-- <th>Delete</th> -->
										</tr>
									</thead>
									<tbody>
										<?php 
										if ($daftar->num_rows()>0) {
											$no=1;
											foreach ($daftar->result() as $lap) { ?>
											<tr id="<?php echo $lap->id_pengirim; ?>">
												<td><?php echo $no; ?></td>
												<td class="nama" contenteditable ori="<?php echo $lap->nama_pengirim; ?>"><?php echo $lap->nama_pengirim; ?></td>
												<td class="telp" contenteditable ori="<?php echo $lap->no_telp_pengirim; ?>"><?php echo $lap->no_telp_pengirim; ?></td>
												<td class="alamat" contenteditable ori="<?php echo $lap->alamat_pengirim; ?>"><?php echo $lap->alamat_pengirim; ?></td>
												<td>
													<button class="border-nol btn btn-success btn-update"><span class="glyphicon glyphicon-pencil"></span>  Update</button>
													<a href="<?php echo base_url('index/hapus_pengirim/'.$lap->id_pengirim); ?>" class="border-nol btn btn-danger btn-delete" onclick="return confirm('Apakah anda yakin ?')"><span class="glyphicon glyphicon-trash"></span> </a>
												</td>
											</tr>
											<?php	
											$no++;
										}
									}else{
										echo "<td colspan='13'>Empty</td>";
									}
									?>
								</tbody>
							</table>
						</div><div class="clearfix"></div>
				</div>
			</div>
		</div>
		<div class="clearfix"></div><br>
		<div class="col-md-12"><a href="<?php echo base_url('index/logout') ?>" class="btn btn-danger btn-lg btn-block" style="border-radius:0px;"><span class="glyphicon glyphicon-off"></span> Logout</a></div>
	</div>
</div>
<div class="footer"><center style="color:#9C9898;">PT. TRANS SARANA JAYA | 2016</center></div>
</body>
</html>
<script>
	$(function() {
		$("#btn-tbp").click(function() {
			$("#tbp").modal('show');
		})
		$('#data-table').dataTable({
		   "aLengthMenu": [ [5, 10, 30, 50, -1], [5, 10, 30, 50, "All"] ],
		   "iDisplayLength": 5,
		   "pagingType": "full_numbers"  
		});
		$(".container").fadeIn('slow');

		$('.btn-update').click(function(){
			var id_pengirim = $(this).closest('tr').attr('id');
			var nama = $('#'+id_pengirim).find('td.nama').text();
			var telp = $('#'+id_pengirim).find('td.telp').text();
			var alamat = $('#'+id_pengirim).find('td.alamat').text();
			var nama_ori = $('#'+id_pengirim).find('td.nama').attr('ori');
			var telp_ori = $('#'+id_pengirim).find('td.telp').attr('ori');
			var alamat_ori = $('#'+id_pengirim).find('td.alamat').attr('ori');
			if (id_pengirim.length != 0 && nama.length != 0 && telp.length != 0 && alamat.length != 0) {
				$.ajax({
					url: "<?php echo base_url('index/daftar_pengirim'); ?>",
					type: 'post',
					data: {nama_pengirim: nama, no_telp_pengirim: telp, alamat_pengirim: alamat, id_pengirim: id_pengirim},
					success: function(x){
						alert('Data Berhasil dirubah.');
						$('#'+id_pengirim).find('td.nama').attr('ori',nama);
						$('#'+id_pengirim).find('td.telp').attr('ori',telp);
						$('#'+id_pengirim).find('td.alamat').attr('ori',alamat);
					}
				});
			}else{
				alert('tidak boleh kosong');
				$('#'+id_pengirim).find('td.nama').text(nama_ori);
				$('#'+id_pengirim).find('td.telp').text(telp_ori);
				$('#'+id_pengirim).find('td.alamat').text(alamat_ori);
			};
		});

	});
	$(function() {
		$('[data-toggle="popover"]').popover();
	})
</script>