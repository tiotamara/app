<!DOCTYPE html>
<html lang="en">
<head>
	<title><?php echo $title; ?></title>
	<link rel="stylesheet" href="<?php echo base_url('dist/css/bootstrap.min.css'); ?>">
	<!-- 	<link rel="stylesheet" href="<?php echo base_url('dist/css/style_prism.css'); ?>"> -->
	<link rel="stylesheet" href="<?php echo base_url('dist/css/chosen.css'); ?>">
	<!-- ================= -->
	<link rel="stylesheet" href="<?php echo base_url('dist/css/custom.css'); ?>">
	<link rel="stylesheet" href="<?php echo base_url('dist/js/jquery/jquery-ui.css'); ?>">
	<!-- <link rel='stylesheet prefetch' href='http://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900|RobotoDraft:400,100,300,500,700,900'> -->
	<link rel='stylesheet prefetch' href='<?php echo base_url('dist/css/font-awesome.min.css'); ?>'>
	<script src='<?php echo base_url('dist/js/jquery.min.js'); ?>'></script>
	<script src='<?php echo base_url('dist/js/jquery/jquery-ui.min.js'); ?>'></script>
	<script src='<?php echo base_url('dist/js/bootstrap.min.js'); ?>'></script>
	<!-- ===================== -->
	<style>
	.border-nol{
		/*border-radius:0px;*/
	}
	.logo:hover{
		box-shadow: 0px 5px 30px -15px #000;
	}
	.padding-nol-kanan{
		padding-right: 0;
	}
	.padding-nol-kiri{
		padding-left: 0;
	}
	</style>
</head>
<body>
	<div class="container">
		<div class="row">
			<a href="<?php echo base_url('index/admin'); ?>"><center><img src="<?php echo base_url('image/logo.jpg') ?>" class="img-responsive logo"></center></a>
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<div class="box">
					<div class="box-icon">
						<span class="fa fa-4x fa-edit"></span>
					</div>
					<div class="info">
						<h4 class="text-center">Edit Data</h4>
					</div>
				</div>
				<!-- FORM -->
				<div class="col-md-12">
					<div class="row">
					<!-- Modal -->
					<div class="modal fade" tabindex="-1" role="dialog" id="tbp">
						<div class="modal-dialog">
							<div class="modal-content border-nol">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-small="Close"><span aria-hidden="true">&times;</span></button>
									<h4 class="modal-title"><span class="glyphicon glyphicon-user"></span> Tambah Pengirim</h4>
								</div>
								<form method="POST" action="<?php echo base_url('index/tambah_pengirim'); ?>">
									<div class="modal-body">
										<div class="form-group">
											<small>Nama</small>
											<input type="text" class="border-nol form-control input-sm" name="nama_pengirim" placeholder="Nama Pengirim" required>
										</div>
										<div class="form-group">
											<small>No. Telpon</small>
											<input type="number" class="border-nol form-control input-sm" name="telp_pengirim" placeholder="No. Telp" required>
										</div>
										<div class="form-group">
											<small>Alamat</small>
											<textarea class="border-nol form-control" name="alamat_pengirim" placeholder="Alamat Pengirim" required></textarea>
										</div>
									</div>
									<div class="modal-footer">
										<button type="submit" class="btn btn-primary border-nol border-nol"><span class="glyphicon glyphicon-send"></span> Tambah</button>
									</div>
								</form>
							</div><!-- /.modal-content -->
						</div><!-- /.modal-dialog -->
					</div><!-- /.modal -->
					<!-- ./Modal -->
						<a href="<?php echo base_url('index/daftar_pengirim') ?>" target="_blank" class="btn btn-info border-nol pull-right"><span class="glyphicon glyphicon-list"></span> Daftar Pengirim</a>
						<button class="btn btn-success border-nol pull-right" data-toggle="modal" data-target="#tbp"><span class="glyphicon glyphicon-plus"></span> Tambah</button>
						<a href="<?php echo base_url('index/laporan') ?>" class="btn btn-danger pull-right radius-nol">Kembali</a>
						<div class="clearfix"></div>
						<section>
							<div class="wizard">
								<form role="form" method="POST" action="">
									<?php $pass = $this->db->query("SELECT secret_password FROM password ")->row(); ?>
									<input type="hidden" autocomplete="off" name="password_edit" class="form-control" style="width:100%;" placeholder="Masukkan Kode" required value="<?php echo $pass->secret_password ?>">
									<div class="tab-content">
										<div class="tab-pane active" role="tabpanel" id="step1">
											<p>Personel</p>
											<select name="nama_personel" data-placeholder="Personel" class="chosen-select form-control" tabindex="4" id="data_kasir">
												<option value=""></option>
												<?php foreach ($option_kasir as $kasir): ?>
													<option value="<?php echo $kasir->nama_kasir; ?>" <?php echo $kasir->nama_kasir==$data->nama_personel ? 'selected="selected"' : ''; ?> ><?php echo ucwords($kasir->nama_kasir); ?></option>
												<?php endforeach ?>
											</select><div class="clearfix"></div><br>
											<?php echo $this->session->flashdata('msg'); ?>
											<h3>Pengirim</h3>
											<div class="col-md-12 padding-nol-kanan padding-nol-kiri">
											<p class="col-md-12 padding-nol-kiri padding-nol-kanan" style="border-bottom:1px solid #eee;">Data Pengirim</p>
												<div class="col-md-6 padding-nol-kiri">
													<select data-placeholder="Search Name" class="chosen-select form-control" tabindex="4" id="data_pengirim">
														<option value=""></option>
														<?php foreach ($option_pengirim as $pengirim): ?>
															<option nama="<?php echo $pengirim->nama_pengirim; ?>" alamat="<?php echo $pengirim->alamat_pengirim; ?>" telp="<?php echo $pengirim->no_telp_pengirim; ?>" value="<?php echo $pengirim->id_pengirim; ?>"><?php echo ucwords($pengirim->nama_pengirim); ?></option>
														<?php endforeach ?>
													</select>
													<div class="clearfix"></div><br>
													<input type="text" value="<?php echo $data->nama_pengirim; ?>" id="nama_pengirim" name="nama_pengirim" autocomplete="off" required placeholder="Nama Pengirim" class="radius-nol form-control" /><br>
													<input type="Number" value="<?php echo $data->no_telp_pengirim; ?>" id="telp_pengirim" name="no_telp_pengirim" autocomplete="off" required placeholder="No Telp" class="radius-nol form-control" /><br>
												</div>
												<div class="col-md-6 padding-nol-kanan">
													<textarea id="alamat_pengirim" required name="alamat_pengirim" cols="30" rows="2" class="radius-nol form-control" placeholder="Alamat Pengirim"><?php echo $data->alamat_pengirim; ?></textarea><br>
												</div>
											</div>
											<div class="col-md-12 padding-nol-kiri padding-nol-kanan">
											<p class="col-md-12 padding-nol-kiri padding-nol-kanan" style="border-bottom:1px solid #eee;">Data Penerima</p>
												<div class="col-md-6 padding-nol-kiri">
													<input type="text" name="nama_penerima" value="<?php echo $data->nama_penerima; ?>" autocomplete="off" required placeholder="Nama Penerima" class="radius-nol form-control" /><br>
													<input type="Number" name="no_telp_penerima" value="<?php echo $data->no_telp_penerima; ?>" autocomplete="off" required placeholder="No Telp" class="radius-nol form-control" /><br>
												</div>
												<div class="col-md-6 padding-nol-kanan">
													<textarea required name="alamat_penerima" cols="30" rows="2" class="radius-nol form-control" placeholder="Alamat Penerima"><?php echo ucwords($data->alamat_penerima); ?></textarea><br>
												</div>
											</div>
											<div class="col-md-12 padding-nol-kiri padding-nol-kanan">
											<p class="col-md-12" style="border-bottom:1px solid #eee;">Data Lain</p>
												<div class="col-md-6 padding-nol-kiri">
													<small>Asal</small>
													<textarea required name="asal" cols="30" rows="2" class="border-nol form-control" placeholder="Asal"><?php echo $data->asal; ?></textarea><br>
													<small>Jenis Pengiriman</small>
													<select class="form-control border-nol" name="jenis_pengiriman">
														<option value="">Jenis Pengiriman</option>
														<option <?php if($data->jenis_pengiriman=='udara'){echo "selected";} ?> value="udara">Udara</option>
														<option <?php if($data->jenis_pengiriman=='darat'){echo "selected";} ?> value="darat">Darat</option>
														<option <?php if($data->jenis_pengiriman=='laut'){echo "selected";} ?> value="laut">Laut</option>
													</select><br>
													<small>Isi Barang</small>
													<input type="text" value="<?php echo $data->isi_barang; ?>" name="isi_barang" autocomplete="off" required placeholder="Isi barang" class="border-nol form-control" /><br>
													<small>Nama Transportasi</small>
													<select class="form-control border-nol" name="nama_pesawat">
														<option value="garuda">Nama Transportasi</option>
														<option value="garuda" disabled>-- Darat --</option>
														<option <?php if($data->nama_pesawat=='truk'){echo "selected='selected'";} ?> value="truk">Truk</option>
														<option value="garuda" disabled>-- Laut --</option>
														<option <?php if($data->nama_pesawat=='kapal'){echo "selected='selected'";} ?> value="kapal">Kapal</option>
														<option value="garuda" disabled>-- Udara --</option>
														<option <?php if($data->nama_pesawat=='air_asia'){echo "selected='selected'";} ?> value="air_asia">Air Asia</option>
														<option <?php if($data->nama_pesawat=='batik_air'){echo "selected='selected'";} ?> value="batik_air">Batik Air</option>
														<option <?php if($data->nama_pesawat=='citi_link'){echo "selected='selected'";} ?> value="citi_link">Citi Link</option>
														<option <?php if($data->nama_pesawat=='garuda'){echo "selected='selected'";} ?> value="garuda">Garuda</option>
														<option <?php if($data->nama_pesawat=='lion_air'){echo "selected='selected'";} ?> value="lion_air">Lion Air</option>
														<option <?php if($data->nama_pesawat=='nam_air'){echo "selected='selected'";} ?> value="nam_air">Nam Air</option>
														<option <?php if($data->nama_pesawat=='sriwijaya'){echo "selected='selected'";} ?> value="sriwijaya">Sriwijaya</option>
														<option <?php if($data->nama_pesawat=='wing_air'){echo "selected='selected'";} ?> value="wing_air">Wing Air</option>
													</select><br>
													<small>Kode Tujuan</small>
													<input type="text" name="kode_airline" value="<?php echo $data->kode_airline; ?>" autocomplete="off" placeholder="Kode Airline" class="form-control border-nol" /><br>
												</div>
												<div class="col-md-6 padding-nol-kanan">
													<small>Tujuan</small>
													<textarea required name="tujuan" cols="30" rows="2" class="form-control border-nol" placeholder="Tujuan"><?php echo $data->tujuan; ?></textarea><br>
													<small>Service</small>
													<select class="form-control border-nol" name="service">
														<option value="door">Service</option>
														<option <?php if($data->service=='door'){echo "selected";} ?> value="door">DOOR</option>
														<option <?php if($data->service=='port'){echo "selected";} ?> value="port">PORT</option>
													</select><br>
													<small>No. SMU</small>
													<input type="text" name="no_smu" value="<?php echo $data->no_smu; ?>" autocomplete="off" required placeholder="No. SMU" class="form-control border-nol" /><br>
													<small>Tanggal berangkat</small>
													<input type="text" name="tanggal_berangkat" value="<?php echo date('m/d/Y',strtotime($data->tanggal_berangkat)); ?>" autocomplete="off" required placeholder="tanggal berangkat" class="form-control border-nol datepicker" /><br>
													<small>Koli</small>
													<input type="number" name="koli" value="<?php echo $data->koli; ?>" autocomplete="off" required placeholder="Koli" class="form-control border-nol" /><br>
												</div>
												<div class="col-md-12 padding-nol-kiri padding-nol-kanan">
												<small>Jenis (Kg/Volume)</small>
												<div class="col-md-4 col-md-offset-4 padding-nol-kiri">
													<select class="form-control" name="type_choice_kg" id="type_choice_kg">
														<option value="0">-Pilih-</option>
														<option value="1" <?php echo $data->type_choice_kg == '1' ? 'selected' : '' ?>>Alternatif</option>
														<option value="0" <?php echo $data->type_choice_kg == '0' ? 'selected' : '' ?>>Manual</option>
													</select>
												</div><div class="clearfix"></div><br>
												<?php if ($data->type_choice_kg == '0'): ?>
													<div class="col-md-6 padding-nol-kiri">
														<small><strong>Alternatif</strong></small><br>
														<!-- <div class="col-md-6 padding-nol-kiri">
															<input id="jumlah_kali_kg" type="number" name="jumlah_kali_kg" autocomplete="off" placeholder="Jumlah kali" class="form-control">
														</div> -->
														<div class="col-md-12 padding-nol-kanan padding-nol-kiri">
															<input id="value_nilai_kg" type="text" name="value_nilai_kg" class="form-control" autocomplete="off" placeholder="Value nilai">
														</div>
													</div>
													<div class="col-md-6">
														<div id="jenis_kg">
															<?php 
															$kg=explode(',', $data->rincian_per_kg);
															 ?>
															<small>Rician/Kg (bila kosong kasih nilai 0)</small><br>
															<div id="place_kg_clone">
																<div class="form-group col-md-2 padding-nol-kiri">
																	<input style="width:100%;display:initial;" type="number" name="rincian_per_kg[]" class="form-control border-nol box1kg" autocomplete="off" placeholder="box 1" value="<?php echo $kg[0]; ?>" required>
																</div>
																<?php if (count($kg) > 1): array_shift($kg); ?>
																	<?php foreach ($kg as $key => $value): ?>
																		<div class="form-group col-md-2 padding-nol-kiri">
																			<input name="rincian_per_kg[]" class="form-control border-nol box1kg" style="width:95%;display:initial;" required="" type="number" placeholder="box 1" value="<?php echo $value ?>" autocomplete="off">
																			<button class="remove btn btn-danger" style="width:95%;">&times;</button>
																		</div>
																	<?php endforeach ?>
																<?php endif ?>
															</div>
															<button type="button" class="btn btn-success btn-block" id="cloning_kg"><span class="glyphicon glyphicon-plus"></span>&nbsp;Tambah</button>
															<small>Berat</small>
															<input type="number" name="kg" autocomplete="off" placeholder="Kg" class="form-control border-nol" id="tempat_kg" value="<?php echo $data->kg; ?>"><br>
															
														</div>
													</div>
												<?php else: ?>
													<div class="col-md-6 padding-nol-kiri">
														<small><strong>Alternatif</strong></small><br>
														<!-- <div class="col-md-6 padding-nol-kiri">
															<input id="jumlah_kali_kg" type="number" name="jumlah_kali_kg" autocomplete="off" placeholder="Jumlah kali" class="form-control" value="<?php echo $data->jumlah_kali_kg; ?>">
														</div> -->
														<div class="col-md-12 padding-nol-kanan padding-nol-kiri">
															<input id="value_nilai_kg" type="text" name="value_nilai_kg" class="form-control" autocomplete="off" placeholder="Value nilai" value="<?php echo $data->value_nilai_kg; ?>">
														</div>
													</div>
													<div class="col-md-6">
														<div id="jenis_kg">
															<small><strong>Rician/Kg (bila kosong kasih nilai 0)</strong></small><br>
															<div id="place_kg_clone">
																<div class="form-group col-md-2 padding-nol-kanan" style="padding-left:0;">
																	<input style="width:95%;display:initial;" type="number" name="rincian_per_kg[]" class="form-control border-nol box1kg" autocomplete="off" placeholder="box 1" value="0" required>
																</div>
															</div>
															<button type="button" class="btn btn-success btn-block" id="cloning_kg"><span class="glyphicon glyphicon-plus"></span>&nbsp;Tambah</button>
															<small>Total Berat (KG)<span style="color:#ff2222;">&nbsp;(Jangan dirubah, karena sudah otomatis!)</span></small>
															<input type="number" style="background: #e6e6e6;border: 1px solid #ff2323;" name="kg" autocomplete="off" placeholder="Kg" class="form-control border-nol" id="tempat_kg"/><br>
															
														</div>
													</div>
												<?php endif ?>
												<div class="col-md-4 col-md-offset-4 padding-nol-kiri">
													<select class="form-control" name="type_choice_vol" id="type_choice_vol">
														<option value="0">-Pilih-</option>
														<option value="1" <?php echo $data->type_choice_vol == '1' ? 'selected' : '' ?>>Alternatif</option>
														<option value="0" <?php echo $data->type_choice_vol == '0' ? 'selected' : '' ?>>Manual</option>
													</select>
												</div><div class="clearfix"></div><br>
												<?php if ($data->type_choice_vol == '0'): ?>
													<div class="col-md-6 padding-nol-kiri">
														<small><strong>Alternatif</strong></small><br>
														<!-- <div class="col-md-6 padding-nol-kiri">
															<input id="jumlah_kali_vol" type="number" name="jumlah_kali_vol" autocomplete="off" placeholder="Jumlah kali" class="form-control">
														</div> -->
														<div class="col-md-12 padding-nol-kanan padding-nol-kiri">
															<input id="value_nilai_vol" type="text" name="value_nilai_vol" class="form-control" autocomplete="off" placeholder="Value nilai">
														</div>
													</div>
													<div class="col-md-6">
														<div id="jenis_vol">
															<?php $vol=explode(',', $data->rincian_per_vol); ?>
															<small>Rician/Vol (bila kosong kasih nilai 0)</small><br>
															<div id="place_vol_clone">
																<div class="form-group col-md-2 padding-nol-kiri">
																	<input style="width:100%;display:initial;" type="number" name="rincian_per_vol[]" class="form-control border-nol box1vol" autocomplete="off" placeholder="box 1" value="<?php echo $vol[0]; ?>" required>
																</div>
																<?php if (count($vol) > 1): array_shift($vol); ?>
																	<?php foreach ($vol as $key => $value): ?>
																		<div class="form-group col-md-2 padding-nol-kiri">
																			<input name="rincian_per_vol[]" class="form-control border-nol box1vol" style="width:95%;display:initial;" required="" type="number" placeholder="box 1" value="<?php echo $value ?>" autocomplete="off">
																			<button class="remove btn btn-danger" style="width:95%;">&times;</button>
																		</div>
																	<?php endforeach ?>
																<?php endif ?>
															</div>
																	<button type="button" class="btn btn-success btn-block" id="cloning_vol"><span class="glyphicon glyphicon-plus"></span>&nbsp;Tambah</button>
															<small>Jumlah Volume</small>
															<input type="number" name="vol" autocomplete="off" placeholder="Volume" class="form-control border-nol" id="tempat_vol" value="<?php echo $data->vol; ?>"/><br>
															<input type="number" style="display:none;" name="tarif_per_vol" autocomplete="off" placeholder="tarif per Volume (IDR)" class="form-control border-nol" value="<?php echo $data->tarif_per_vol; ?>"/><br>
														</div>
													</div>
												<?php else: ?>
													<div class="col-md-6 padding-nol-kiri">
														<small><strong>Alternatif</strong></small><br>
														<!-- <div class="col-md-6 padding-nol-kiri">
															<input id="jumlah_kali_vol" type="number" name="jumlah_kali_vol" autocomplete="off" placeholder="Jumlah kali" class="form-control" value="<?php echo $data->jumlah_kali_vol; ?>">
														</div> -->
														<div class="col-md-12 padding-nol-kanan padding-nol-kiri">
															<input id="value_nilai_vol" type="text" name="value_nilai_vol" class="form-control" autocomplete="off" placeholder="Value nilai" value="<?php echo $data->value_nilai_vol ?>">
														</div>
													</div>
													<div class="col-md-6">
														<div id="jenis_vol">
															<small><strong>Rician/Vol (bila kosong kasih nilai 0)</strong></small><br>
															<div id="place_vol_clone">
																<div class="form-group col-md-2 padding-nol-kanan" style="padding-left:0;">
																	<input style="width:100%;display:initial;" type="number" name="rincian_per_vol[]" class="form-control border-nol box1vol" autocomplete="off" placeholder="box 1" value="0" required>
																</div>
															</div>
															<button type="button" class="btn btn-success btn-block" id="cloning_vol"><span class="glyphicon glyphicon-plus"></span>&nbsp;Tambah</button>
															<small>Total Berat (Volume)<span style="color:#ff2222;">&nbsp;(Jangan dirubah, karena sudah otomatis!)</span></small>
															<input type="number" style="background: #e6e6e6;border: 1px solid #ff2323;" name="vol" autocomplete="off" placeholder="Volume" class="form-control border-nol" id="tempat_vol"/><br>
															<input type="number" style="display:none;" name="tarif_per_vol" autocomplete="off" placeholder="tarif per Volume (IDR)" class="form-control border-nol" /><br>
															
														</div>
													</div>
												<?php endif ?>
												<!-- ================================================================== -->
													<div id="jenis_vol">
														<div class="col-md-6 padding-nol-kiri">
															<h5>Chargeable Weight</h5>
															<input id="chargeable" type="number" name="chargeable" autocomplete="off" placeholder="Berat yang dikenakan Biaya" class="form-control border-nol" value="<?php echo $data->chargeable ?>"/>
														</div>
														<div class="col-md-6 padding-nol-kanan">
															<h5>Tarif/Kg</h5>
															<input id="tarif" type="number" name="tarif_per_kg" autocomplete="off" placeholder="tarif per Kg (IDR)" class="form-control border-nol" value="<?php echo $data->tarif_per_kg ?>"/>
														</div><div class="clearfix"></div><br>
													</div>
												</div>
											</div>
											<div class="col-md-12">
												<div class='col-md-12'>
													<h3>Biaya Lain</h3>
													<p>Biaya-biaya lain</p>
												</div>
												<div class="col-md-6">
													<small>Biaya Karantina</small>
													<input id="karantina" type="number" name="karantina" autocomplete="off" required placeholder="Biaya Karantina" class="radius-nol form-control" value="<?php echo $data->karantina ?>" /><br>
													<small>Biaya Pick Up</small>
													<input id="pickup" type="Number" name="pick_up" autocomplete="off" required placeholder="Biaya Pick up" class="radius-nol form-control" value="<?php echo $data->pick_up ?>" /><br>
													<small>Biaya Packing</small>
													<input id="packing" type="Number" name="packing" autocomplete="off" required placeholder="Biaya Packing" class="radius-nol form-control" value="<?php echo $data->packing ?>" /><br>
													<small>Service Charge</small>
													<input id="charge" type="Number" name="service_charge" autocomplete="off" required placeholder="Service Charge" class="radius-nol form-control" value="<?php echo $data->service_charge ?>" /><br>
												</div>
												<div class="col-md-6">
													<small>Biaya Handling</small>
													<input id="handling" type="Number" name="handling" autocomplete="off" required placeholder="Biaya Handling" class="radius-nol form-control" value="<?php echo $data->handling ?>" /><br>
													<small>Biaya lain-lain</small>
													<input id="lain" type="Number" name="lain_lain" autocomplete="off" required placeholder="Biaya lain-lain" class="radius-nol form-control" value="<?php echo $data->lain_lain ?>" /><br>
													<small>Biaya Adm. SMU</small>
													<input id="smu" type="Number" name="adm_smu" autocomplete="off" required placeholder="Biaya Adm. SMU" class="radius-nol form-control" value="<?php echo $data->adm_smu ?>" /><br>
												</div>
												<div class="col-md-12">
													<small>Subtotal</small>
													<input type="text" name="subtotall" autocomplete="off" required placeholder="Subtotal" class="radius-nol form-control" id="subtotal" disabled="disabled" value="<?php echo $data->subtotal ?>"/><br>
													<input type="hidden" name="subtotal" autocomplete="off" required placeholder="Subtotal" class="radius-nol form-control" id="subtotal_hide" value="<?php echo $data->subtotal ?>"/><br>
													<small>Cara Pembayaran</small>
													<select class="form-control border-nol" id="cara_pembayaran" name="cara_pembayaran">
														<option value="cash">cara pembayaran</option>
														<option <?php if($data->cara_pembayaran=='cash'){echo "selected";} ?> value="cash">Cash</option>
														<option <?php if($data->cara_pembayaran=='kredit'){echo "selected";} ?> value="kredit">Kredit</option>
													</select><br>
													<div id="dp_kredit" style="display:none;">
														<small>Jumlah DP</small>
														<input type="Number" name="dp_kredit" autocomplete="off" placeholder="DP kredit" class="radius-nol form-control"  value="<?php echo $data->dp_kredit ?>" /><br>
													</div>
												</div>
											</div>
											<ul class="list-inline pull-right">
												
												<li><button type="submit" class="btn btn-warning btn-lg btn-block">Revisi</button></li>
											</ul>
										</div>
										<div class="clearfix"></div>
									</div>
								</form>
							</div>
						</section>
					</div>
				</div>
				<!-- END FORM -->
			</div><div class="clearfix"></div><br>
			<div class="col-md-12"><a href="<?php echo base_url('index/logout') ?>" class="btn btn-danger btn-lg btn-block" style="border-radius:0px;"><span class="glyphicon glyphicon-off"></span> Logout</a></div>
		</div>
	</div>
	<div class="footer"><center style="color:#9C9898;">PT. TRANS SARANA JAYA | 2016</center></div>
</body>
<script src="<?php echo base_url('dist/js/chosen.jquery.js'); ?>" type="text/javascript"></script>
<script src="<?php echo base_url('dist/js/prism.js'); ?>" type="text/javascript" charset="utf-8"></script>
<script>
function summ() {
	$("#subtotal").val(Number($("#chargeable").val())*Number($("#tarif").val())+Number($("#karantina").val())+Number($("#pickup").val())+Number($("#packing").val())+Number($("#charge").val())+Number($("#handling").val())+Number($("#lain").val())+Number($("#smu").val()));
	$("#subtotal_hide").val(Number($("#chargeable").val())*Number($("#tarif").val())+Number($("#karantina").val())+Number($("#pickup").val())+Number($("#packing").val())+Number($("#charge").val())+Number($("#handling").val())+Number($("#lain").val())+Number($("#smu").val()));
}
	$(document).ready(function () {
		$("#type_choice_kg").change(function() {
			if ($("#type_choice_kg").val() == '0') {
				$("#jumlah_kali_kg").attr('disabled','disabled');
				$("#value_nilai_kg").attr('disabled','disabled');
				$("#cloning_kg").removeAttr('disabled','disabled');
			}else{
				$("#jumlah_kali_kg").removeAttr('disabled','disabled');
				$("#value_nilai_kg").removeAttr('disabled','disabled');
				$("#cloning_kg").attr('disabled','disabled');
			}
		})

		$("#type_choice_vol").change(function() {
			if ($("#type_choice_vol").val() == '0') {
				$("#jumlah_kali_vol").attr('disabled','disabled');
				$("#value_nilai_vol").attr('disabled','disabled');
				$("#cloning_vol").removeAttr('disabled','disabled');
			}else{
				$("#jumlah_kali_vol").removeAttr('disabled','disabled');
				$("#value_nilai_vol").removeAttr('disabled','disabled');
				$("#cloning_vol").attr('disabled','disabled');
			}
		})
		$("html, body").animate({
		       scrollTop: 0
		   }, 1000);
		$("#chargeable").change(function() {
			summ();
			// alert($("#subtotal").val());
		});
		$("#tarif").change(function() {
			summ();
		});
		$("#karantina").change(function() {
			summ();
		});
		$("#packing").change(function() {
			summ();
		});
		$("#pickup").change(function() {
			summ();
		});
		$("#charge").change(function() {
			summ();
		});
		$("#handling").change(function() {
			summ();
		});
		$("#lain").change(function() {
			summ();
		});
		$("#smu").change(function() {
			summ();
		});
			$( ".datepicker" ).datepicker();
	        var sum=0;
	        var sum_vol=0;

	    	// clone
	    	//Clone and Remove Form Fields

	    	$('#cloning_kg').on('click', function() { 
	    		// alert('ayee');
	    	    $('#place_kg_clone').append('<div class="form-group col-md-2 padding-nol-kiri"><input style="width:95%;display:initial;" type="number" name="rincian_per_kg[]" class="form-control border-nol box1kg" autocomplete="off" placeholder="box 1" value="0" required><button class="remove btn btn-danger" style="width:95%;">&times;</button></div>');
	    	    return false; //prevent form submission
	    	});

	    	$('#place_kg_clone').on('click', '.remove', function() {
	    	    $(this).parent().remove();
	    	    sum=0;
	    	    $(".box1kg").each(function() {
	    	    		sum = sum + Number($(this).val());
	    	      });
	    	    $("#tempat_kg").val(sum);
	    	    return false; //prevent form submission
	    	});
	    	// end clone

	    	// clone
	    	//Clone and Remove Form Fields

	    	$('#cloning_vol').on('click', function() { 
	    		// alert('ayee');
	    	    $('#place_vol_clone').append('<div class="form-group col-md-2 padding-nol-kiri"><input style="width:95%;display:initial;" type="number" name="rincian_per_vol[]" class="form-control border-nol box1vol" autocomplete="off" placeholder="box 1" value="0" required><button class="remove btn btn-danger" style="width:95%;">&times;</button></div>');
	    	    return false; //prevent form submission
	    	});

	    	$('#place_vol_clone').on('click', '.remove', function() {
	    	    $(this).parent().remove();
	    	    sum_vol=0;
	    	    $(".box1vol").each(function() {
	    	    		sum_vol = sum_vol + Number($(this).val());
	    	      });
	    	    $("#tempat_vol").val(sum_vol);
	    	    return false; //prevent form submission
	    	});
	    	$('#place_kg_clone').on('change','.box1kg',function() {
	    		sum=0;
	    		$(".box1kg").each(function() {
	    				sum = sum + Number($(this).val());
	    		  });
	    		$("#tempat_kg").val(sum);
	    		// alert($(this).val());
	    	    // $(this).parent().remove();
	    	    // return false; //prevent form submission
	    	});
	    	$('#place_vol_clone').on('change','.box1vol',function() {
	    		sum_vol=0;
	    		$(".box1vol").each(function() {
	    				sum_vol = sum_vol + Number($(this).val());
	    		  });
	    		$("#tempat_vol").val(sum_vol);
	    	});
	    	// end clone
	    //Initialize tooltips
	    $('.nav-tabs > li a[title]').tooltip();
	    
	    //Wizard
	    $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {

	    	var $target = $(e.target);

	    	if ($target.parent().hasClass('disabled')) {
	    		return false;
	    	}
	    });

	    if ($("#cara_pembayaran option:selected").val() == 'cash') {
	    		$("#dp_kredit").fadeOut();
	    	}else{
	    		$("#dp_kredit").fadeIn();
	    	};

	    $("#cara_pembayaran").change(function() {
	    	if ($("#cara_pembayaran option:selected").val() == 'cash') {
	    		$("#dp_kredit").fadeOut();
	    	}else{
	    		$("#dp_kredit").fadeIn();
	    	};
	    })

	    $(".next-step").click(function (e) {

	    	var $active = $('.wizard .nav-tabs li.active');
	    	$active.next().removeClass('disabled');
	    	nextTab($active);

	    });
	    $(".prev-step").click(function (e) {

	    	var $active = $('.wizard .nav-tabs li.active');
	    	prevTab($active);

	    });

	    $('#data_pengirim').on('change', function(){
	    	var ti = $(this).val();
	    	var obj = $(this).find("option[value="+ti+"]");
	    	$('#nama_pengirim').val(obj.attr('nama'));
	    	$('#alamat_pengirim').val(obj.attr('alamat'));
	    	$('#telp_pengirim').val(obj.attr('telp'));
	    });

	});

	function nextTab(elem) {
		$(elem).next().find('a[data-toggle="tab"]').click();
	}
	function prevTab(elem) {
		$(elem).prev().find('a[data-toggle="tab"]').click();
	}
	$(".chosen-select").chosen({no_results_text: "Oops, nothing found!",width:"100%"}); 
</script>
</html>
