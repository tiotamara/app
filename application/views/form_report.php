<!DOCTYPE html>
<html lang="en">
<head>
	<title><?php echo $title; ?></title>
	<link rel="stylesheet" href="<?php echo base_url('dist/css/bootstrap.min.css'); ?>">
	<!-- 	<link rel="stylesheet" href="<?php echo base_url('dist/css/style_prism.css'); ?>"> -->
	<link rel="stylesheet" href="<?php echo base_url('dist/css/chosen.css'); ?>">
	<!-- ================= -->
	<link rel="stylesheet" href="<?php echo base_url('dist/css/custom.css'); ?>">
	<link rel="stylesheet" href="<?php echo base_url('dist/js/jquery/jquery-ui.css'); ?>">
	<!-- <link rel='stylesheet prefetch' href='http://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900|RobotoDraft:400,100,300,500,700,900'> -->
	<link rel='stylesheet prefetch' href='<?php echo base_url('dist/css/font-awesome.min.css'); ?>'>
	<script src='<?php echo base_url('dist/js/jquery.min.js'); ?>'></script>
	<script src='<?php echo base_url('dist/js/jquery/jquery-ui.min.js'); ?>'></script>
	<script src='<?php echo base_url('dist/js/bootstrap.min.js'); ?>'></script>
	<!-- ===================== -->
	<style>
	.border-nol{
		/*border-radius:0px;*/
	}
	.logo:hover{
		box-shadow: 0px 5px 30px -15px #000;
	}
	.padding-nol-kanan{
		padding-right: 0;
	}
	.padding-nol-kiri{
		padding-left: 0;
	}
	</style>
</head>
<body>
	<div class="container">
		<div class="row">
			<a href="<?php echo base_url('index/admin'); ?>"><center><img src="<?php echo base_url('image/logo.jpg') ?>" class="img-responsive logo"></center></a>
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<div class="box">
					<div class="box-icon">
						<span class="fa fa-4x fa-keyboard-o"></span>
					</div>
					<div class="info">
						<h4 class="text-center">Input Form</h4>
					</div>
				</div>
				<!-- FORM -->
				<div class="col-md-12">
					<div class="row">
						<section>
							<div class="wizard">
								<!-- Modal -->
								<div class="modal fade" tabindex="-1" role="dialog" id="tbp">
									<div class="modal-dialog">
										<div class="modal-content border-nol">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
												<h6 style="font-weight: normal;"><span class="glyphicon glyphicon-user"></span> Tambah Pengirim</h6>
											</div>
											<form method="POST" action="<?php echo base_url('index/tambah_pengirim'); ?>">
												<div class="modal-body">
													<div class="form-group">
														<!-- <label>Nama</label> -->
														<input type="text" class="border-nol form-control input-sm" name="nama_pengirim" placeholder="Nama Pengirim" required>
													</div>
													<div class="form-group">
														<!-- <label>No. Telpon</label> -->
														<input type="number" class="border-nol form-control input-sm" name="telp_pengirim" placeholder="No. Telp" required>
													</div>
													<div class="form-group">
														<!-- <label>Alamat</label> -->
														<textarea class="border-nol form-control" name="alamat_pengirim" placeholder="Alamat Pengirim" required></textarea>
													</div>
												</div>
												<div class="modal-footer">
													<button type="submit" class="btn btn-primary border-nol border-nol"><span class="glyphicon glyphicon-send"></span> Tambah</button>
												</div>
											</form>
										</div><!-- /.modal-content -->
									</div><!-- /.modal-dialog -->
								</div><!-- /.modal -->
								<!-- ./Modal -->
								<form role="form" target="_blank" method="POST" action="<?php echo base_url('index/saved'); ?>">
									<div class="tab-content">
										<div class="tab-pane active" role="tabpanel">

											<a href="<?php echo base_url('index/daftar_pengirim') ?>" target="blank" class="btn btn-info border-nol pull-right"><span class="glyphicon glyphicon-list"></span> Daftar Pengirim</a>
											<button type="button" class="btn btn-success border-nol pull-right" data-toggle="modal" data-target="#tbp"><span class="glyphicon glyphicon-plus"></span> Tambah</button><div class="clearfix"></div>
											<div class="col-md-12"><br>
												<p>Personel</p>
												<select name="nama_personel" data-placeholder="Personel" class="chosen-select form-control" tabindex="4" id="data_kasir">
													<option value=""></option>
													<?php foreach ($option_kasir as $kasir): ?>
														<option value="<?php echo $kasir->nama_kasir; ?>"><?php echo ucwords($kasir->nama_kasir); ?></option>
													<?php endforeach ?>
												</select><div class="clearfix"></div><br>
												<div class="col-md-12" style="padding-left:0;">
													<h3>Pengirim</h3>
													<p>Data Pengirim</p>
												</div>
												<div class="col-md-6 padding-nol-kiri">
													<select data-placeholder="Search Name" class="chosen-select form-control" tabindex="4" id="data_pengirim">
														<option value=""></option>
														<?php foreach ($option_pengirim as $pengirim): ?>
															<option nama="<?php echo $pengirim->nama_pengirim; ?>" alamat="<?php echo $pengirim->alamat_pengirim; ?>" telp="<?php echo $pengirim->no_telp_pengirim; ?>" value="<?php echo $pengirim->id_pengirim; ?>"><?php echo ucwords($pengirim->nama_pengirim); ?></option>
														<?php endforeach ?>
													</select><div class="clearfix"></div><br>
													<input type="text" id="nama_pengirim" name="nama_pengirim" autocomplete="off" required placeholder="Nama Pengirim" class="radius-nol form-control" /><br>
													<input type="Number" id="telp_pengirim" name="no_telp_pengirim" autocomplete="off" required placeholder="No Telp" class="radius-nol form-control" /><br>
												</div>
												<div class="col-md-6 padding-nol-kanan">
													<textarea id="alamat_pengirim" required name="alamat_pengirim" cols="30" rows="5" class="radius-nol form-control" placeholder="Alamat Pengirim"></textarea><br>
												</div>
												<!-- ============= -->
												<div class="col-md-12 padding-nol-kanan" style="padding-left:0;">
													<div class='col-md-12 padding-nol-kanan' style="padding-left:0;">
														<h3>Penerima</h3>
														<p>Data Penerima</p>
													</div>
													<div class="col-md-6" style="padding-left:0;">
														<input type="text" name="nama_penerima" autocomplete="off" required placeholder="Nama Penerima" class="radius-nol form-control" /><br>
														<input type="Number" name="no_telp_penerima" autocomplete="off" required placeholder="No Telp" class="radius-nol form-control" /><br>
													</div>
													<div class="col-md-6 padding-nol-kanan">
														<textarea required name="alamat_penerima" cols="30" rows="3" class="radius-nol form-control" placeholder="Alamat Penerima"></textarea><br>
													</div>
												</div>
													<div class="col-md-12 padding-nol-kanan" style="padding-left:0;">
														<div class="col-md-12 padding-nol-kanan" style="padding-left:0;">
															<h3>Data</h3>
															<p>Data Lain</p>
														</div>
														<div class="col-md-12 padding-nol-kanan" style="padding-left:0;">
															<div class="col-md-6 padding-nol-kiri">
																<small>Asal</small>
																<input required name="asal" class="border-nol form-control" placeholder="Asal" id="asal">
															</div>
															<div class="col-md-6 padding-nol-kanan">
																<small>Tujuan</small>
																<input required name="tujuan" class="form-control border-nol" placeholder="Tujuan" id="tujuan">
															</div><div class="clearfix"></div><br>
															<div class="col-md-6 padding-nol-kiri">
																<small>Jenis Pengiriman</small>
																<select class="form-control border-nol" name="jenis_pengiriman">
																	<option value="udara">Jenis Pengiriman</option>
																	<option value="udara">Udara</option>
																	<option value="darat">Darat</option>
																	<option value="laut">Laut</option>
																</select>
															</div>
															<div class="col-md-6 padding-nol-kanan">
																<small>Service</small>
																<select class="form-control border-nol" name="service">
																	<option value="door">Service</option>
																	<option value="door">DOOR</option>
																	<option value="port">PORT</option>
																</select>
															</div><div class="clearfix"></div><br>
															<div class="col-md-6 padding-nol-kiri">
																<small>Isi Barang</small>
																<input type="text" name="isi_barang" autocomplete="off" required placeholder="Isi barang" class="border-nol form-control" />
															</div>
															<div class="col-md-6 padding-nol-kanan">
																<small>Nama Transportasi</small>
																<select class="form-control border-nol" name="nama_pesawat">
																	<option value="garuda">Nama Transportasi</option>
																	<option value="garuda" disabled>-- Darat --</option>
																	<option value="truk">Truk</option>
																	<option value="garuda" disabled>-- Laut --</option>
																	<option value="kapal">Kapal</option>
																	<option value="garuda" disabled>-- Udara --</option>
																	<option value="air_asia">Air Asia</option>
																	<option value="batik_air">Batik Air</option>
																	<option value="citi_link">Citi Link</option>
																	<option value="garuda">Garuda</option>
																	<option value="lion_air">Lion Air</option>
																	<option value="nam_air">Nam Air</option>
																	<option value="sriwijaya">Sriwijaya</option>
																	<option value="wing_air">Wing Air</option>
																	<option value="express_air">Express Air</option>
																</select>
															</div><div class="clearfix"></div><br>
															<div class="col-md-6 padding-nol-kiri">
																<small>No. SMU</small>
																<input type="text" name="no_smu" autocomplete="off" required placeholder="No. SMU" class="form-control border-nol" />
															</div>
															<div class="col-md-6 padding-nol-kanan">
																<small>Kode Tujuan</small>
																<input type="text" name="kode_airline" autocomplete="off" placeholder="Kode Tujuan" class="form-control border-nol" />
															</div><div class="clearfix"></div><br>
															<div class="col-md-6 padding-nol-kiri">
																<small>Tanggal berangkat</small>
																<input type="text" name="tanggal_berangkat" autocomplete="off" required placeholder="tanggal berangkat" class="form-control border-nol datepicker" />
															</div>
															<div class="col-md-6 padding-nol-kanan">
																<small>Koli</small>
																<input type="number" name="koli" autocomplete="off" required placeholder="Koli" class="form-control border-nol" />
															</div><div class="clearfix"></div><br>
														</div>
														<div class="col-md-12 padding-nol-kanan" style="padding-left:0;">
															<div class="col-md-4 col-md-offset-4 padding-nol-kiri">
																<select class="form-control" name="type_choice_kg" id="type_choice_kg">
																	<option value="0">-Pilih-</option>
																	<option value="1">Alternatif</option>
																	<option value="0">Manual</option>
																</select>
															</div><div class="clearfix"></div><br>
															<div class="col-md-6 padding-nol-kiri">
																<small><strong>Alternatif</strong></small><br>
																<!-- <div class="col-md-6 padding-nol-kiri">
																	<input id="jumlah_kali_kg" type="number" name="jumlah_kali_kg" autocomplete="off" placeholder="Jumlah kali" class="form-control">
																</div> -->
																<div class="col-md-12 padding-nol-kiri padding-nol-kanan">
																	<input id="value_nilai_kg" type="text" name="value_nilai_kg" class="form-control" autocomplete="off" placeholder="Value Alternatif">
																</div>
															</div>
															<div class="col-md-6">
																<div id="jenis_kg">
																	<small><strong>Rician/Kg (bila kosong kasih nilai 0)</strong></small><br>
																	<div id="place_kg_clone">
																		<div class="form-group col-md-2 padding-nol-kanan" style="padding-left:0;">
																			<input style="width:95%;display:initial;" type="number" name="rincian_per_kg[]" class="form-control border-nol box1kg" autocomplete="off" placeholder="box 1" value="0" required>
																		</div>
																	</div>
																	<button type="button" class="btn btn-success btn-block" id="cloning_kg"><span class="glyphicon glyphicon-plus"></span>&nbsp;Tambah</button>
																	<small>Total Berat (KG)<span style="color:#ff2222;">&nbsp;(Jangan dirubah, karena sudah otomatis!)</span></small>
																	<input type="number" style="background: #e6e6e6;border: 1px solid #ff2323;" name="kg" autocomplete="off" placeholder="Kg" class="form-control border-nol" id="tempat_kg"/><br>
																	
																</div>
															</div>
															<div class="col-md-4 col-md-offset-4 padding-nol-kiri">
																<select class="form-control" name="type_choice_vol" id="type_choice_vol">
																	<option value="0">-Pilih-</option>
																	<option value="1">Alternatif</option>
																	<option value="0">Manual</option>
																</select>
															</div><div class="clearfix"></div><br>
															<div class="col-md-6 padding-nol-kiri">
																<small><strong>Alternatif</strong></small><br>
																<!-- <div class="col-md-6 padding-nol-kiri">
																	<input id="jumlah_kali_vol" type="number" name="jumlah_kali_vol" autocomplete="off" placeholder="Jumlah kali" class="form-control">
																</div> -->
																<div class="col-md-12 padding-nol-kanan padding-nol-kiri">
																	<input id="value_nilai_vol" type="text" name="value_nilai_vol" class="form-control" autocomplete="off" placeholder="Value Alternatif">
																</div>
															</div>
															<div class="col-md-6">
																<div id="jenis_vol">
																	<small><strong>Rician/Vol (bila kosong kasih nilai 0)</strong></small><br>
																	<div id="place_vol_clone">
																		<div class="form-group col-md-2 padding-nol-kanan" style="padding-left:0;">
																			<input style="width:100%;display:initial;" type="number" name="rincian_per_vol[]" class="form-control border-nol box1vol" autocomplete="off" placeholder="box 1" value="0" required>
																		</div>
																	</div>
																	<button type="button" class="btn btn-success btn-block" id="cloning_vol"><span class="glyphicon glyphicon-plus"></span>&nbsp;Tambah</button>
																	<small>Total Berat (Volume)<span style="color:#ff2222;">&nbsp;(Jangan dirubah, karena sudah otomatis!)</span></small>
																	<input type="number" style="background: #e6e6e6;border: 1px solid #ff2323;" name="vol" autocomplete="off" placeholder="Volume" class="form-control border-nol" id="tempat_vol"/><br>
																	<input type="number" style="display:none;" name="tarif_per_vol" autocomplete="off" placeholder="tarif per Volume (IDR)" class="form-control border-nol" /><br>
																	
																</div>
															</div>
															<div class="col-md-6 padding-nol-kiri">
																<h5>Chargeable Weight</h5>
																<input id="chargeable" type="number" name="chargeable" autocomplete="off" placeholder="Berat yang dikenakan Biaya" class="form-control border-nol" />
															</div>
															<div class="col-md-6 padding-nol-kanan">
																<h5>Tarif/Kg</h5>
																<input id="tarif" type="number" name="tarif_per_kg" autocomplete="off" placeholder="tarif per Kg (IDR)" class="form-control border-nol" />
															</div><div class="clearfix"></div><br>
														</div>
													</div>
													<div class="col-md-12">
														<div class='col-md-12'>
															<h3>Biaya Lain</h3>
															<p>Biaya-biaya lain</p>
														</div>
														<div class="col-md-6">
															<small>Biaya Karantina</small>
															<input id="karantina" type="number" name="karantina" autocomplete="off" required placeholder="Biaya Karantina" class="radius-nol form-control" value="0" /><br>
															<small>Biaya Ppn</small>
															<input id="pickup" type="Number" name="pick_up" autocomplete="off" required placeholder="Biaya Pick up" class="radius-nol form-control" value="0" /><br>
															<small>Biaya Packing</small>
															<input id="packing" type="Number" name="packing" autocomplete="off" required placeholder="Biaya Packing" class="radius-nol form-control" value="0" /><br>
															<small>Service Charge</small>
															<input id="charge" type="Number" name="service_charge" autocomplete="off" required placeholder="Service Charge" class="radius-nol form-control" value="0" /><br>
														</div>
														<div class="col-md-6">
															<small>Biaya Gudang</small>
															<input id="handling" type="Number" name="handling" autocomplete="off" required placeholder="Biaya Handling" class="radius-nol form-control" value="0" /><br>
															<small>Biaya lain-lain</small>
															<input id="lain" type="Number" name="lain_lain" autocomplete="off" required placeholder="Biaya lain-lain" class="radius-nol form-control" value="0" /><br>
															<small>Biaya Adm. SMU</small>
															<input id="smu" type="Number" name="adm_smu" autocomplete="off" required placeholder="Biaya Adm. SMU" class="radius-nol form-control" value="0" /><br>
														</div>
														<div class="col-md-12">
															<small>Subtotal</small>
															<input type="text" name="subtotall" autocomplete="off" required placeholder="Subtotal" class="radius-nol form-control" value="0" id="subtotal" disabled="disabled" /><br>
															<input type="hidden" name="subtotal" autocomplete="off" required placeholder="Subtotal" class="radius-nol form-control" value="0" id="subtotal_hide" /><br>
															<small>Cara Pembayaran</small>
															<select class="form-control border-nol" id="cara_pembayaran" name="cara_pembayaran">
																<option value="cash">cara pembayaran</option>
																<option value="cash">Cash</option>
																<option value="kredit">Kredit</option>
															</select><br>
															<div id="dp_kredit" style="display:none;">
																<small>Jumlah DP</small>
																<input type="Number" name="dp_kredit" autocomplete="off" placeholder="DP kredit" class="radius-nol form-control" value="0" /><br>
															</div>
														</div>
													</div>
													<ul class="list-inline pull-right">
														<small>Apakah Transaksi Anda Sudah Benar ?</small>
														<li><button type="button" id="reload" value="convert" class="btn btn-primary btn-lg btn-block"><span class="glyphicon glyphicon-refresh"></span>&nbsp;Invoice Baru</button></li>
														<li><button type="submit" id="saved" value="convert" class="btn btn-success btn-lg btn-block"><span class="glyphicon glyphicon-print"></span>&nbsp;Ya &amp; Print</button></li>
													</ul>
											</div>
										</div>
										<div class="clearfix"></div>
									</div>
								</form>
							</div>
						</section>
					</div>
				</div>
				<!-- END FORM -->
			</div><div class="clearfix"></div><br>
			<div class="col-md-12"><a href="<?php echo base_url('index/logout') ?>" class="btn btn-danger btn-lg btn-block" style="border-radius:0px;"><span class="glyphicon glyphicon-off"></span> Logout</a></div>
		</div>
	</div>
	<div class="footer"><center style="color:#9C9898;">PT. TRANS SARANA JAYA | 2016</center></div>
</body>
<?php 
// echo print_r($provinsi->result());
	$data_provinsi = array();
	foreach ($provinsi->result() as $data) {
		array_push($data_provinsi, $data->name);
	}
 ?>
<script src="<?php echo base_url('dist/js/chosen.jquery.js'); ?>" type="text/javascript"></script>
<script src="<?php echo base_url('dist/js/prism.js'); ?>" type="text/javascript" charset="utf-8"></script>
<script>
$("#saved").click(function() {
	window.location.href="<?php echo base_url(); ?>";
})
function summ() {
	$("#subtotal").val(Number($("#chargeable").val())*Number($("#tarif").val())+Number($("#karantina").val())+Number($("#pickup").val())+Number($("#packing").val())+Number($("#charge").val())+Number($("#handling").val())+Number($("#lain").val())+Number($("#smu").val()));
	$("#subtotal_hide").val(Number($("#chargeable").val())*Number($("#tarif").val())+Number($("#karantina").val())+Number($("#pickup").val())+Number($("#packing").val())+Number($("#charge").val())+Number($("#handling").val())+Number($("#lain").val())+Number($("#smu").val()));
}

	$(document).ready(function () {
		$("#type_choice_kg").change(function() {
			if ($("#type_choice_kg").val() == '0') {
				// $("#jumlah_kali_kg").attr('disabled','disabled');
				$("#value_nilai_kg").attr('disabled','disabled');
				$("#cloning_kg").removeAttr('disabled','disabled');
			}else{
				// $("#jumlah_kali_kg").removeAttr('disabled','disabled');
				$("#value_nilai_kg").removeAttr('disabled','disabled');
				$("#cloning_kg").attr('disabled','disabled');
			}
		})

		$("#type_choice_vol").change(function() {
			if ($("#type_choice_vol").val() == '0') {
				// $("#jumlah_kali_vol").attr('disabled','disabled');
				$("#value_nilai_vol").attr('disabled','disabled');
				$("#cloning_vol").removeAttr('disabled','disabled');
			}else{
				// $("#jumlah_kali_vol").removeAttr('disabled','disabled');
				$("#value_nilai_vol").removeAttr('disabled','disabled');
				$("#cloning_vol").attr('disabled','disabled');
			}
		})

		$("#chargeable").change(function() {
			summ();
			// alert($("#subtotal").val());
		});
		$("#tarif").change(function() {
			summ();
		});
		$("#karantina").change(function() {
			summ();
		});
		$("#packing").change(function() {
			summ();
		});
		$("#pickup").change(function() {
			summ();
		});
		$("#charge").change(function() {
			summ();
		});
		$("#handling").change(function() {
			summ();
		});
		$("#lain").change(function() {
			summ();
		});
		$("#smu").change(function() {
			summ();
		});

		$("html, body").animate({
		       scrollTop: 0
		   }, 1000);
		$("#bank").focus();

		$("#reload").click(function() {
			location.reload();
		})
		$( ".datepicker" ).datepicker();
		var availableTags = <?php echo json_encode($data_provinsi); ?>;
		    $( "#asal" ).autocomplete({
		      source: availableTags
		    });
		    $( "#tujuan" ).autocomplete({
		      source: availableTags
		    });

	    var sum=0;
	    var sum_vol=0;

		// clone
		//Clone and Remove Form Fields

		$('#cloning_kg').on('click', function() { 
			// alert('ayee');
		    $('#place_kg_clone').append('<div class="form-group col-md-2 padding-nol-kanan padding-nol-kiri"><input style="width:95%;display:initial;" type="number" name="rincian_per_kg[]" class="form-control border-nol box1kg" autocomplete="off" placeholder="box 1" value="0" required><button class="remove btn btn-danger" style="width:95%;">&times;</button></div>');
		    return false; //prevent form submission
		});

		$('#place_kg_clone').on('click', '.remove', function() {
		    $(this).parent().remove();
		    sum=0;
		    $(".box1kg").each(function() {
		    		sum = sum + Number($(this).val());
		      });
		    $("#tempat_kg").val(sum);
		    return false; //prevent form submission
		});
		// end clone

		// clone
		//Clone and Remove Form Fields

		$('#cloning_vol').on('click', function() { 
			// alert('ayee');
		    $('#place_vol_clone').append('<div class="form-group col-md-2 padding-nol-kanan padding-nol-kiri"><input style="width:95%;display:initial;" type="number" name="rincian_per_vol[]" class="form-control border-nol box1vol" autocomplete="off" placeholder="box 1" value="0" required><button class="remove btn btn-danger" style="width:95%;">&times;</button></div>');
		    return false; //prevent form submission
		});

		$('#place_vol_clone').on('click', '.remove', function() {
		    $(this).parent().remove();
		    sum_vol=0;
		    $(".box1vol").each(function() {
		    		sum_vol = sum_vol + Number($(this).val());
		      });
		    $("#tempat_vol").val(sum_vol);
		    return false; //prevent form submission
		});
		// end clone
	    //Initialize tooltips
	    $('.nav-tabs > li a[title]').tooltip();
	    
	    //Wizard
	    $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {

	    	var $target = $(e.target);

	    	if ($target.parent().hasClass('disabled')) {
	    		return false;
	    	}
	    });

	    $("#cara_pembayaran").change(function() {
	    	if ($("#cara_pembayaran option:selected").val() == 'cash') {
	    		$("#dp_kredit").fadeOut();
	    	}else{
	    		$("#dp_kredit").fadeIn();
	    	};
	    })
	    	// $("#place_kg_clone").on('change','.rincian_per_kg',function() {
	    	// 	sum += $(this).val();
	    	// 	alert(sum);
	    	// })
	    	$('#place_kg_clone').on('change','.box1kg',function() {
	    		sum=0;
	    		$(".box1kg").each(function() {
	    				sum = sum + Number($(this).val());
	    		  });
	    		$("#tempat_kg").val(sum);
	    		// alert($(this).val());
	    	    // $(this).parent().remove();
	    	    // return false; //prevent form submission
	    	});
	    	$('#place_vol_clone').on('change','.box1vol',function() {
	    		sum_vol=0;
	    		$(".box1vol").each(function() {
	    				sum_vol = sum_vol + Number($(this).val());
	    		  });
	    		$("#tempat_vol").val(sum_vol);
	    	});

	    $(".next-step").click(function (e) {

	    	var $active = $('.wizard .nav-tabs li.active');
	    	$active.next().removeClass('disabled');
	    	nextTab($active);

	    });
	    $(".prev-step").click(function (e) {

	    	var $active = $('.wizard .nav-tabs li.active');
	    	prevTab($active);

	    });

	    $('#data_pengirim').on('change', function(){
	    	var ti = $(this).val();
	    	var obj = $(this).find("option[value="+ti+"]");
	    	$('#nama_pengirim').val(obj.attr('nama'));
	    	$('#alamat_pengirim').val(obj.attr('alamat'));
	    	$('#telp_pengirim').val(obj.attr('telp'));
	    });

	});

	function nextTab(elem) {
		$(elem).next().find('a[data-toggle="tab"]').click();
	}
	function prevTab(elem) {
		$(elem).prev().find('a[data-toggle="tab"]').click();
	}
	$(".chosen-select").chosen({no_results_text: "Oops, nothing found!",width:"100%"}); 
</script>
</html>
