<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<title><?php echo $title; ?></title>
	<link rel="stylesheet" href="<?php echo base_url('dist/css/bootstrap.min.css'); ?>">
	<!-- ================= -->
    <link rel="stylesheet" href="<?php echo base_url('dist/css/reset.css'); ?>">
	<!-- <link rel='stylesheet prefetch' href='http://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900|RobotoDraft:400,100,300,500,700,900'> -->
	<link rel='stylesheet prefetch' href='<?php echo base_url('dist/css/font-awesome.min.css'); ?>'>
	<link rel="stylesheet" href="<?php echo base_url('dist/css/style.css'); ?>">
	<script src='<?php echo base_url('dist/js/jquery.min.js'); ?>'></script>
	<!-- ===================== -->
	<style>
	body{
		padding-top: 20px;
	}
	#overlay_loading{ 
	    display: none; 
	    position: fixed;
	    left: 0px;
	    top: 0px;
	    width: 100%;
	    height: 100%;
	    z-index: 9999;
	    /*background-color: transparent;*/
	    background: url(<?php echo base_url().'/image/loading.gif' ?>) 50% 49% no-repeat rgba(95, 82, 82, 0.58);
	}
	.parallax {
	  height: 600px;
	  background-position: 50% 50%;
	  background-repeat: no-repeat;
	  background-attachment: fixed;
	  background-size: cover;
	}
	.parallax-1 {
	  background-image: url(<?php echo base_url().'image/background.jpg' ?>);
	}
	</style>
</head>
<body class="parallax parallax-1">
	<div id="overlay_loading"></div>
	<div class="container">
		<div class="row">
			<!-- ======================================= -->
			<!-- <div class="pen-title">
			  <h1>Test</h1>
			</div> -->
			<div class="rerun"><a href="#0">PT. TRANS SARANA JAYA</a></div>
			<div class="container" style="display:none;">
			  <div class="card"></div>
			  <div class="card">
			    <h1 class="title">Login</h1>
			      <div class="input-container">
			        <input type="text" id="Username"/>
			        <label for="Username">Username</label>
			        <div class="bar"></div>
			      </div>
			      <div class="input-container">
			        <input type="password" id="Password"/>
			        <label for="Password">Password</label>
			        <div class="bar"></div>
			      </div>
			      <div class="button-container">
			        <button id="loginn"><span>Go</span></button>
			      </div>
			      <div class="footer" style="font-size:13px;">PT. TRANS SARANA JAYA | 2016</div>
			  </div>
			  <div class="card alt" title="Tentang Perusahaan">
			    <i class="fa fa-building-o toggle"></i></i>
			    <h1 class="title"><i class="fa fa-building-o"></i> About
			      <div class="close"></div>
			    </h1>
			  </div>
			</div>
			<!-- ========================================== -->
		</div>
	</div>
</body>
<script>
$(function() {
	$(".container").fadeIn('slow');
	$('#Password').keypress(function( ev ){
	    if(ev.which == 13){
	        ev.preventDefault();
	        $("#loginn").click();
	    }
	})
});
	$("#loginn").click(function() {
		if ($("#Username").val().length == 0 || $("#Password").val().length == 0) {
			alert('Lengkapi Username dan Password !');
			$("#Username").focus();
		}else{
			$("#overlay_loading").fadeIn('slow');
			$.ajax({
				type:'post',
				url:"<?php echo base_url('index/login'); ?>",
				data:{username: $('#Username').val(),password: $('#Password').val()},
				 success : function(data) {
				  alert(data);
				  $("#overlay_loading").fadeOut('slow');
				  if (data=='berhasil') {
				  	window.location.reload();
				  };
				}
			});
		};
	})
</script>
<script src="<?php echo base_url('dist/js/index.js'); ?>"></script>
</html>