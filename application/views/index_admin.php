<!DOCTYPE html>
<html lang="en">
<head>
	<title><?php echo $title; ?></title>
	<link rel="stylesheet" href="<?php echo base_url('dist/css/bootstrap.min.css'); ?>">
	<!-- ================= -->
    <link rel="stylesheet" href="<?php echo base_url('dist/css/custom.css'); ?>">
	<!-- <link rel='stylesheet prefetch' href='http://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900|RobotoDraft:400,100,300,500,700,900'> -->
	<link rel='stylesheet prefetch' href='<?php echo base_url('dist/css/font-awesome.min.css'); ?>'>
	<script src='<?php echo base_url('dist/js/jquery.min.js'); ?>'></script>
	<!-- ===================== -->
	<style>
		.logo:hover{
			box-shadow: 0px 5px 30px -15px #000;
		}
		#videosList {
		 max-width: 600px; 
		  overflow: hidden;
		}

		.video {
		  background-image: url('http://apps.wonderfulisland.id/wp-content/uploads/2014/09/video-placeholder-1-1.jpg');
		  height: 330px;
		  width: 600px;
		  margin-bottom: 50px;
		  overflow: hidden;
		}

		.size-video{
			height: 380px;
		  	width: 677px;
		}

		/* Hide Play button + controls on iOS */
		video::-webkit-media-controls {
		    display:none !important;
		}
	</style>
</head>
<body>
	<div class="container">
	    <div class="row">
	        <a href="<?php echo base_url('index/admin'); ?>"><center><img src="<?php echo base_url('image/logo.jpg') ?>" class="img-responsive logo"></center></a>
	        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
	            <div class="box">
	                <div class="box-icon">
	                    <span class="fa fa-4x fa-history"></span>
	                </div>
	                <div class="info">
	                    <h4 class="text-center">History</h4>
	                    <p>Report data history</p>
	                    <a href="<?php echo base_url('index/laporan'); ?>" class="btn btn-block"><span class="glyphicon glyphicon-send"></span> Go</a>
	                </div>
	            </div>
	        </div>
	        
	        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
	            <div class="box">
	                <div class="box-icon">
	                    <span class="fa fa-4x fa-keyboard-o"></span>
	                </div>
	                <div class="info">
	                    <h4 class="text-center">Input Form</h4>
	                    <p>Membuat report baru</p>
	                    <a href="<?php echo base_url('index/form'); ?>" class="btn btn-block"><span class="glyphicon glyphicon-send"></span> Go</a>
	                </div>
	            </div>
	        </div>
	        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
	        	<div class="box">
	        	    <div class="box-icon">
	        	        <span class="fa fa-4x fa-users"></span>
	        	    </div>
	        	    <div class="info">
	        	        <h4 class="text-center">Pengirim</h4>
	        	        <p>Data Pengirim</p>
	        	        <a href="<?php echo base_url('index/daftar_pengirim'); ?>" class="btn btn-block"><span class="glyphicon glyphicon-send"></span> Go</a>
	        	    </div>
	        	</div>
	        	<!-- <div id="videosList">           

	        	<div class="video">
	        	    <video class="thevideo size-video" loop preload="none">
	        	      <source src="http://apps.wonderfulisland.id/wp-content/uploads/2014/09/woman-using-phone-1.mp4" type="video/mp4">
	        	      <source src="http://apps.wonderfulisland.id/wp-content/uploads/2014/09/woman-using-phone.webm">
	        	    Your browser does not support the video tag.
	        	    </video>
	        	  </div>
	        	</div> -->
	        </div><div class="clearfix"></div><br>
	        <div class="col-md-12"><a href="<?php echo base_url('index/logout') ?>" class="btn btn-danger btn-lg btn-block" style="border-radius:0px;"><span class="glyphicon glyphicon-off"></span> Logout</a></div>
		</div>
	</div>
		<div class="footer"><center style="color:#9C9898;">PT. TRANS SARANA JAYA | 2016</center></div>
</body>
</html>
<script>
	$(function() {
		var figure = $(".video").hover( hoverVideo, hideVideo );

		function hoverVideo(e) {  
		    $('video', this).get(0).play(); 
		}

		function hideVideo(e) {
		    $('video', this).get(0).pause(); 
		}
		$(".container").fadeIn('slow');
	});
</script>