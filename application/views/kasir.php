<!DOCTYPE html>
<html lang="en">
<head>
	<title><?php echo $title; ?></title>
	<link rel="stylesheet" href="<?php echo base_url('dist/css/bootstrap.min.css'); ?>">
	<!-- 	<link rel="stylesheet" href="<?php echo base_url('dist/css/style_prism.css'); ?>"> -->
	<link rel="stylesheet" href="<?php echo base_url('dist/css/chosen.css'); ?>">
	<!-- ================= -->
	<link rel="stylesheet" href="<?php echo base_url('dist/css/custom.css'); ?>">
	<link rel="stylesheet" href="<?php echo base_url('dist/js/jquery/jquery-ui.css'); ?>">
	<!-- <link rel='stylesheet prefetch' href='http://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900|RobotoDraft:400,100,300,500,700,900'> -->
	<link rel='stylesheet prefetch' href='<?php echo base_url('dist/css/font-awesome.min.css'); ?>'>
	<link rel='stylesheet prefetch' href='<?php echo base_url('dist/DataTables/datatables.css'); ?>'>
	<script src='<?php echo base_url('dist/js/jquery.min.js'); ?>'></script>
	<script src='<?php echo base_url('dist/DataTables/datatables.min.js'); ?>'></script>
	<script src='<?php echo base_url('dist/js/jquery/jquery-ui.min.js'); ?>'></script>
	<script src='<?php //echo base_url('dist/js/bootstrap.min.js'); ?>'></script>
	<!-- ===================== -->
	<style>
		.logo:hover{
			box-shadow: 0px 5px 30px -15px #000;
		}
	</style>
</head>
<body>
	<div class="container">
		<div class="row">
			<a href="<?php echo base_url('index/admin'); ?>"><center><img src="<?php echo base_url('image/logo.jpg') ?>" class="img-responsive logo"></center></a>
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<!-- Button trigger modal -->
				<button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">
				  Tambah Personel
				</button>

				<!-- Modal -->
				<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
				  <div class="modal-dialog" role="document">
				    <div class="modal-content">
				      <div class="modal-header">
				        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				        <h4 class="modal-title" id="myModalLabel">Tambah Personel</h4>
				      </div>
				      <div class="modal-body">
				        <form action="<?php echo base_url('index/tambah_kasir'); ?>" method="POST">
				        	<input type="text" name="nama_kasir" autocomplete="off" class="form-control" id="nama_kasir" placeholder="Nama Kasir" required><br>
				        	<button class="btn btn-primary" type="submit">Simpan</button>
				        	<button class="btn btn-danger" type="reset">Reset</button>
				        </form>
				      </div>
				    </div>
				  </div>
				</div>
				<div class="box">
					<div class="box-icon">
						<span class="fa fa-4x fa-history"></span>
					</div>
					<div class="info">
						<h4 class="text-center">Kasir</h4>
						<p>Data Personel</p><hr>
							<table class="table table-hover" style="text-align: left;" id="data-table">
								<thead>
									<tr>
										<th>No</th>
										<th>Nama</th>
										<th>Delete</th>
									</tr>
								</thead>
								<tbody>
										<?php 
										if ($kasir->num_rows()>0) {
												$no=1;
												foreach ($kasir->result() as $data): ?>
											<tr>
													<td><?php echo $no; $no++;?></td>
													<td><?php echo $data->nama_kasir; ?></td>
													<td><a onclick="return confirm('Apakah anda yakin ?')" href="<?php echo base_url('index/delete_kasir/'.$data->id_kasir); ?>" class="btn btn-danger"><span class="glyphicon glyphicon-trash"></span></a></td>
											</tr>
												<?php endforeach ?>
										<?php }
										 ?>
								</tbody>
							</table>
					</div>
				</div>
			</div>
		</div>
		<div class="clearfix"></div><br>
		<div class="col-md-12"><a href="<?php echo base_url('index/logout') ?>" class="btn btn-danger btn-lg btn-block" style="border-radius:0px;"><span class="glyphicon glyphicon-off"></span> Logout</a></div>
	</div>
</div>
<div class="footer"><center style="color:#9C9898;">PT. TRANS SARANA JAYA | 2016</center></div>
</body>
</html>
<script>
	$("#edit").click(function() {
		$("#form-edit").slideToggle('slow');
	})
	$("#myModal").on("shown.bs.modal",function() {
	    $("#nama_kasir").focus();
	});
</script>