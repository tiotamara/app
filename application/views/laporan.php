<!DOCTYPE html>
<html lang="en">
<head>
	<link rel="stylesheet" href="<?php echo base_url('dist/css/bootstrap.min.css'); ?>">
	<!-- 	<link rel="stylesheet" href="<?php echo base_url('dist/css/style_prism.css'); ?>"> -->
	<link rel="stylesheet" href="<?php echo base_url('dist/css/chosen.css'); ?>">
	<!-- ================= -->
	<link rel="stylesheet" href="<?php echo base_url('dist/css/custom.css'); ?>">
	<link rel="stylesheet" href="<?php echo base_url('dist/js/jquery/jquery-ui.css'); ?>">
	<!-- <link rel='stylesheet prefetch' href='http://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900|RobotoDraft:400,100,300,500,700,900'> -->
	<link rel='stylesheet prefetch' href='<?php echo base_url('dist/css/font-awesome.min.css'); ?>'>
	<link rel='stylesheet prefetch' href='<?php echo base_url('dist/DataTables/datatables.css'); ?>'>
	<script src='<?php echo base_url('dist/js/jquery.min.js'); ?>'></script>
	<script src='<?php echo base_url('dist/DataTables/datatables.min.js'); ?>'></script>
	<script src='<?php echo base_url('dist/js/jquery/jquery-ui.min.js'); ?>'></script>
	<script src='<?php //echo base_url('dist/js/bootstrap.min.js'); ?>'></script>
	<!-- ===================== -->
	<style>
		.logo:hover{
			box-shadow: 0px 5px 30px -15px #000;
		}
	</style>
</head>
<body>
	<div class="container">
		<div class="row">
			<a href="<?php echo base_url('index/admin'); ?>"><center><img src="<?php echo base_url('image/logo.jpg') ?>" class="img-responsive logo"></center></a>
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<div class="box">
					<div class="box-icon">
						<span class="fa fa-4x fa-history"></span>
					</div>
					<div class="info">
						<h4 class="text-center">History</h4>
						<p>Report data history</p><hr>
						<?php echo $this->session->flashdata('msg'); ?>
						<!-- TABS -->
						<div>

						  <!-- Nav tabs -->
						  <ul class="nav nav-tabs" role="tablist">
						    <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab"><span class="glyphicon glyphicon-search"></span>&nbsp;Cari Laporan</a></li>
						    <!-- <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab"><span class="glyphicon glyphicon-trash"></span>&nbsp;Delete Laporan Multiple</a></li> -->
						    <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab"><span class="glyphicon glyphicon-floppy-disk"></span>&nbsp;Buat LPH</a></li>
						  </ul>

						  <!-- Tab panes -->
						  <div class="tab-content">
						    <div role="tabpanel" class="tab-pane active" id="home">
						    	<div class="clearfix"></div><br>
			    	    		<form action="<?php echo base_url('index/laporan'); ?>" method="POST">
			    		    		<div class="col-md-4" style="padding-left:0;"><input style="border-radius:0;" type="text" name="tgl1" placeholder="Tanggal Awal" class="form-control datepicker" autocomplete="off"></div>
			    		    		<div class="col-md-2"><center>Sampai</center><hr></div>
			    		    		<div class="col-md-4"><input style="border-radius:0;" type="text" name="tgl2" placeholder="Tanggal Akhir" class="form-control datepicker" autocomplete="off"></div>
			    		    		<div class="col-md-2" style="padding-right:0;"><button type="submit" class="btn btn-success btn-block" style="border-radius:0;"><span class="glyphicon glyphicon-search"></span>&nbsp;Cari</button></div>
			    		    		<div class="clearfix"></div><br>
			    	    		</form>
						    </div>
						    <div role="tabpanel" class="tab-pane" id="profile">
						    	<div class="clearfix"></div><br>
						    	<!-- <button class="btn btn-danger pull-right" id="delete_multiple"><span class="glyphicon glyphicon-trash"></span> Delete by Date</button><div class="clearfix"></div><br> -->
						    	<!-- <div id="delete_multiple_date" style="display:none;"> -->
						    		<form action="<?php echo base_url('index/delete_multiple'); ?>" method="POST">
							    		<div class="col-md-4" style="padding-left:0;"><input style="border-radius:0;" type="text" name="tgl_delete1" placeholder="Tanggal Awal" class="form-control datepicker" autocomplete="off"></div>
							    		<div class="col-md-2"><center>Sampai</center><hr></div>
							    		<div class="col-md-4"><input style="border-radius:0;" type="text" name="tgl_delete2" placeholder="Tanggal Akhir" class="form-control datepicker" autocomplete="off"></div>
							    		<div class="col-md-2" style="padding-right:0;"><button onclick="return confirm('Apakah anda yakin ?')" type="submit" class="btn btn-danger btn-block" style="border-radius:0;"><span class="glyphicon glyphicon-trash"></span>&nbsp;Delete</button></div>
							    		<div class="clearfix"></div><br>
						    		</form>
						    	<!-- </div> -->
						    </div>
						    <div role="tabpanel" class="tab-pane" id="messages">
						    <div class="clearfix"></div><br>
						    	<select style="border-radius:0;" id="type" class="form-control">
						    		<option value="print">-- Pilih type laporan --</option>
						    		<option value="print">Print</option>
						    		<option value="excel">Excel</option>
						    	</select><div class="clearfix"></div><br>
						    	<div class="col-md-4" style="padding-left:0;"><input style="border-radius:0;" type="text" id="tgl1" placeholder="Tanggal Awal" class="form-control datepicker" required autocomplete="off"></div>
						    	<div class="col-md-2"><center>Sampai</center><hr></div>
						    	<div class="col-md-4"><input style="border-radius:0;" type="text" id="tgl2" placeholder="Tanggal Akhir" class="form-control datepicker" required autocomplete="off"></div>
						    	<div class="col-md-2" style="padding-right:0;"><a href="#go" id="go" class="btn btn-success btn-block"><span class="glyphicon glyphicon-floppy-disk"></span>&nbsp;Go</a></div>
						    	<div class="clearfix"></div><br>
						    </div>
						  </div>

						</div>
						<!-- END TABS -->
						<div class="table-responsive">
								<table class="table table-hover" style="text-align: left;" id="data-table">
									<thead>
										<tr>
											<th>No</th>
											<th>Invoice</th>
											<th>Tujuan</th>
											<th>Tonase/Kg</th>
											<th>Biaya Lain</th>
											<th>Jumlah</th>
											<th>Cash</th>
											<th>Kredit</th>
											<th>Rek. Bank</th>
											<th>P/D</th>
											<th>A/C</th>
											<th>SMU. No</th>
											<!-- <th colspan="2">Keterangan</th> -->
											<th>Shipper</th>
											<th>Komoditi</th>
											<th>Print</th>
											<th>Excel</th>
											<th>Edit</th>
											<th>Delete</th>
										</tr>
										<!-- <tr>
											<th>Shipper</th>
											<th>Komoditi</th>
										</tr> -->
									</thead>
									<tbody>
										<?php 
										$total=array();
										$total_cash=array();
										$total_credit=array();
										if ($laporan->num_rows()>0) {
											$no=1;
											foreach ($laporan->result() as $lap) { ?>
											<tr>
												<td><?php echo $no; ?></td>
												<td><?php echo $lap->id_penerima; ?></td>
												<td><?php echo $lap->tujuan; ?></td>
												<td><?php echo $lap->kg; ?></td>
												<?php $biayalain=$lap->karantina+$lap->pick_up+$lap->packing+$lap->handling+$lap->lain_lain+$lap->adm_smu+$lap->service_charge; ?>
												<td><?php echo number_format($biayalain,0,',','.'); ?></td>
												<td><?php 
													echo number_format($lap->subtotal,0,',','.');
													array_push($total_cash, $lap->subtotal); 
												
												 ?></td>
												<?php if ($lap->cara_pembayaran == 'cash'): ?>
													<td><?php 
													echo number_format($lap->subtotal,0,',','.');
														array_push($total, $lap->subtotal); 
													
													 ?></td>
													<td>-</td>
												<?php else: ?>
													<td><?php echo number_format($lap->dp_kredit,0,',','.'); ?></td>
													<td><?php 
														array_push($total, $lap->dp_kredit); 
														echo number_format($lap->subtotal-$lap->dp_kredit,0,',','.');
														array_push($total_credit, $lap->subtotal-$lap->dp_kredit); 
													
													 ?></td>
												<?php endif ?>
												<td><?php echo $lap->no_rek; ?></td>
												<td><?php echo $lap->service; ?></td>
												<td>
													<?php 
													if ($lap->nama_pesawat=='nam_air') {
														echo "IN";
													}elseif ($lap->nama_pesawat=='garuda') {
														echo "GA";
													}elseif ($lap->nama_pesawat=='lion_air') {
														echo "JT";
													}elseif ($lap->nama_pesawat=='sriwijaya') {
														echo "SJ";
													}elseif ($lap->nama_pesawat=='citi_link') {
														echo "QG";
													}elseif ($lap->nama_pesawat=='air_asia') {
														echo "QZ";
													}elseif ($lap->nama_pesawat=='wing_air') {
														echo "IW";
													}elseif ($lap->nama_pesawat=='batik_air') {
														echo "ID";
													}elseif ($lap->nama_pesawat=='truk') {
														echo "TRK";
													}elseif ($lap->nama_pesawat=='kapal') {
														echo "KPL";
													}
													 ?>
												</td>
												<td><?php echo $lap->no_smu; ?></td>
												<td><?php echo $lap->nama_pengirim; ?></td>
												<td><small><?php echo $lap->isi_barang; ?></small></td>
												<td><a href="<?php echo base_url('index/out/'.$lap->id_penerima); ?>" target="_blank"><span class="glyphicon glyphicon-print"></span></a></td>
												<td><a href="<?php echo base_url('index/out_xlx/'.$lap->id_penerima); ?>"><i class="fa fa-file-excel-o"></i></a></td>
												<td>
													<button type="button" data-toggle="modal" data-target="#myEdit<?php echo $no; ?>"><span class="glyphicon glyphicon-pencil" title="Edit"></span></button>
													<!-- Modal -->
													<div tabindex="-1" role="dialog" aria-labelledby="myModalLabel" class="modal fade" id="myEdit<?php echo $no; ?>">
													  <div class="modal-dialog" role="document">
													    <div class="modal-content">
													      <div class="modal-header">
													        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
													      </div>
													       	<form method='POST' action="<?php echo base_url('index/edit_data/'.$lap->id_penerima); ?>">
													      <div class="modal-body">
													       		<input type="password" autocomplete="off" name="password_edit" class="form-control" style="width:100%;" placeholder="Masukkan Kode" required>
													      </div>
													      <div class="modal-footer">
													        <button type="submit" class="btn btn-primary">Submit</button>
													      </div>
													       	</form>
													    </div>
													  </div>
													</div>
												</td>
												<td>
													<button type="button" data-toggle="modal" data-target="#myModal<?php echo $no; ?>"><span class="glyphicon glyphicon-trash" title="Delete"></span></button>

													<!-- Modal -->
													<div tabindex="-1" role="dialog" aria-labelledby="myModalLabel" class="modal fade" id="myModal<?php echo $no; ?>">
													  <div class="modal-dialog" role="document">
													    <div class="modal-content">
													      <div class="modal-header">
													        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
													      </div>
													       	<form method='POST' action="<?php echo base_url('index/hapus_data/'.$lap->id_penerima); ?>">
													      <div class="modal-body">
													       		<input type="password" autocomplete="off" name="password_delete" class="form-control" style="width:100%;" placeholder="Masukkan Kode" required>
													      </div>
													      <div class="modal-footer">
													        <button type="submit" class="btn btn-primary">Submit</button>
													      </div>
													       	</form>
													    </div>
													  </div>
													</div>
												</td>
											</tr>
											<?php	
											$no++;
										}
									}else{
										echo "<td colspan='13'>Empty</td>";
									}
									?>
								</tbody>
							</table>
						</div><br>
							<?php echo "<span class='text-center'>Total Keseluruhan Transaksi = Rp. ".number_format(array_sum($total_cash),0,',','.')."</span>"; ?><br>
							<?php echo "<span class='text-center'>Total Transaksi Cash = Rp. ".number_format(array_sum($total),0,',','.')."</span>"; ?><br>
							<?php echo "<span class='text-center'>Total Transaksi Credit = Rp. ".number_format(array_sum($total_credit),0,',','.')."</span>"; ?>
						<div class="clearfix"></div><br>
				</div>
			</div>
		</div>
		<div class="clearfix"></div><br>
		<div class="col-md-12"><a href="<?php echo base_url('index/logout') ?>" class="btn btn-danger btn-lg btn-block" style="border-radius:0px;"><span class="glyphicon glyphicon-off"></span> Logout</a></div>
	</div>
</div>
<div class="footer"><center style="color:#9C9898;">PT. TRANS SARANA JAYA | 2016</center></div>
</body>
</html>
<script>
	$(function() {
		$( ".datepicker" ).datepicker();
		$(".container").fadeIn('slow');
		$('#data-table').dataTable({
		   "aLengthMenu": [ [5, 10, 30, 50, -1], [5, 10, 30, 50, "All"] ],
		   "iDisplayLength": 5,
		   "pagingType": "full_numbers"  
		});
		// $("#delete_multiple").click(function() {
		// 	$("#delete_multiple_date").slideToggle('slow');
		// })
		$('#go').click(function(){
			var type = $("#type option:selected").val();
			var theurl = "<?php echo base_url('index/printlaporan'); ?>";

			var tgl1split = $('#tgl1').val().split('/');
			tgl1 = tgl1split[2]+"-"+tgl1split[0]+"-"+tgl1split[1];
			var tgl2split = $('#tgl2').val().split('/');
			tgl2 = tgl2split[2]+"-"+tgl2split[0]+"-"+tgl2split[1];
			if (tgl1.length==0) {
				// alert(theurl+'/'+tgl);
				alert('Isikan tanggal awal.');
				$('#tgl1').focus();
			}else if(tgl2.length==0){
				alert('Isikan tanggal akhir.');
				$('#tgl2').focus();
			}else{
				window.open(theurl+'/'+tgl1+'/'+tgl2+'?type='+type,'_blank');
			};
		});
	});
</script>