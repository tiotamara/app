<?php

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title><?php echo $title; ?></title>
	<link rel="stylesheet" href="<?php echo base_url('dist/css/bootstrap.min.css'); ?>">
	<!-- ================= -->
	<link rel="stylesheet" href="<?php echo base_url('dist/css/custom.css'); ?>">
	<!-- <link rel='stylesheet prefetch' href='http://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900|RobotoDraft:400,100,300,500,700,900'> -->
	<link rel='stylesheet prefetch' href='<?php echo base_url('dist/css/font-awesome.min.css'); ?>'>
	<script src='<?php echo base_url('dist/js/jquery.min.js'); ?>'></script>
	<!-- ===================== -->
	<style>
		td {    width: 60px;}
		body{
			margin-top: -1.3cm;
			margin-bottom: -1.3cm;
			font-size: 10px;
		}
		.table-bordered>tbody>tr>td.semua {
			border-bottom:0 !important;
			border-top:0 !important;
			border-left:0 !important;
			border-right:0 !important;
		}
		.table-bordered>tbody>tr>td.atas{
			border-top:0 !important;
		}
		.table-bordered>tbody>tr>td.bawah{
			border-bottom:0 !important;
		}
		.table-bordered>tbody>tr>td.kanan{
			border-right:0 !important;
		}
		.table-bordered>tbody>tr>td.kiri{
			border-left:0 !important;
		}
	</style>
	<style type="text/css">
		@media print {
			.table-bordered>tbody>tr>td.semua {
				border-bottom:0 !important;
				border-top:0 !important;
				border-left:0 !important;
				border-right:0 !important;
			}
		}
		@media print {
			.table-bordered>tbody>tr>td.atas{
				border-top:0 !important;
			}
		}
		@media print {
			.table-bordered>tbody>tr>td.bawah{
				border-bottom:0 !important;
			}
		}
		@media print {
			.table-bordered>tbody>tr>td.kanan{
				border-right:0 !important;
			}
		}
		@media print {
			.table-bordered>tbody>tr>td.kiri{
				border-left:0 !important;
			}
		}
	</style>
</head>
<body>

	<?php 
	if ($data->num_rows() > 0) {
		foreach ($data->result() as $data_print) { ?>
			<div class="container">
				<table style="width:100%;" class="table-hover table-bordered">
					<tbody>
						<tr>
							<td width="20"></td>
							<td width="20"></td>
							<td width="20"></td>
							<td width="20"></td>
							<td width="20"></td>
							<td width="20"></td>
							<td width="20"></td>
							<td width="20"></td>
							<td width="20"></td>
							<td width="20"></td>
							<td width="20"></td>
							<td width="20"></td>
							<td width="20"></td>
							<td width="20"></td>
							<td width="20"></td>
							<td width="20"></td>
							<td width="20"></td>
							<td width="20"></td>
							<td width="20"></td>
							<td width="20"></td>
							<td width="20"></td>
							<td width="20"></td>
						</tr>
						<tr>
							<td style="vertical-align:middle;border-right:0 !important;" colspan="2" rowspan="3"> <img class="img-responsive" style="width:100px;" src="<?php echo base_url('image/logo.jpg'); ?>" alt="Logo"> </td>
							<td style="vertical-align:middle;border-left:0 !important;border-bottom:0 !important;" colspan="16"><h4 style="margin:0;"><font face="Baskerville Old Face">PT. TRANS SARANA JAYA</font></h4></td>
							<td style="text-align:center;" colspan="4"><h5 style="margin:0;">INVOICE NO.</h5></td>
						</tr>
						<!-- ===================================== -->
						<tr>
							<td colspan="16" style="border-left:0 !important;border-top:0 !important;border-bottom:0 !important;">JL. NGEKSIGONDO NO.55, KOTAGEDE (DEPAN RS KIA Permata Bunda) YOGYAKARTA</td>
							<td style="font-size:20px;" colspan="4" class="text-center" rowspan="2"><strong><?php echo $data_print->id_penerima; ?></strong></td>
							<!-- <td style="text-align:center; font-size:15px;" colspan="3"></td> -->
						</tr>
						<!-- ===================================== -->
						<tr>
							<td colspan="16" style="border-left:0 !important;border-top:0 !important;">No. CS : <strong>0823 2741 6666 / 0857 2844 4427</strong>  &nbsp;&nbsp;&nbsp;No. Fax : 0274 2840296</td>
						</tr>
						<!-- ===================================== -->
						<tr>
							<td colspan="9" style="border-bottom:0 !important;"><h5 style="margin:0;">PENGIRIM</h5></td>
							<td colspan="9" style="border-bottom:0 !important;"><h5 style="margin:0;">PENERIMA</h5></td>
							<td style="text-align:center;" colspan="2">ASAL</td>
							<td style="text-align:center;" colspan="2"><?php echo ucwords($data_print->asal) ?></td>
						</tr>
						<!-- ===================================== -->
						<tr>
							<td colspan="1" class="bawah" style="border-top:0 !important;border-right:0 !important;">Nama</td>
							<td colspan="8" class="bawah" style="width: 6088px;border-top:0 !important;border-left:0 !important;">: <?php echo ucwords($data_print->nama_pengirim) ?></td>
							<td colspan="1" class="bawah" style="border-top:0 !important;border-right:0 !important;">Nama</td>
							<td colspan="8" class="bawah" style="border-top:0 !important;border-left:0 !important;">: <?php echo ucwords($data_print->nama_penerima) ?></td>
							<td style="text-align:center;" colspan="2">TUJUAN</td>
							<td style="text-align:center;" colspan="2"><b><?php echo strtoupper($data_print->kode_airline) ?></b></td>
						</tr>
						<!-- ===================================== -->
						<tr>
							<td colspan="1" class="atas kanan bawah">Alamat</td>
							<td colspan="8" rowspan="3" valign="top" class="atas kiri bawah">: <?php echo ucwords($data_print->alamat_pengirim) ?></td>
							<td colspan="1" class="atas kanan bawah">Alamat</td>
							<td colspan="8" rowspan="3" valign="top" class="atas kiri bawah">: <?php echo ucwords($data_print->alamat_penerima) ?></td>
							<td style="text-align:center;" colspan="4"><b><?php echo strtoupper($data_print->tujuan) ?></b></td>
						</tr>
						<!-- ===================================== -->
						<tr>
							<td colspan="1" rowspan="2" class="atas kanan bawah"></td>
							<td colspan="1" rowspan="2" class="atas kanan bawah"></td>
							<td style="text-align:center;" colspan="4">Jenis Pengiriman</td>
						</tr>
						<!-- ===================================== -->
						<tr>
							<td style="text-align:center;" colspan="2" rowspan="2"><?php echo strtoupper($data_print->jenis_pengiriman); ?></td>
							<td style="text-align:center;" colspan="2" rowspan="2"><?php echo strtoupper($data_print->service); ?></td>
						</tr>
						<!-- ===================================== -->
						<tr>
							<td colspan="1"  class="atas kanan">Telp.</td>
							<td colspan="8" class="atas kiri">: <?php echo ucwords($data_print->no_telp_pengirim) ?></td>
							<td colspan="1" class="atas kanan">Telp.</td>
							<td colspan="8" class="atas kiri">: <?php echo ucwords($data_print->no_telp_penerima) ?></td>
						</tr>
						<!-- ===================================== -->
						<tr>
							<td style="text-align:center;" colspan="4"><b>Isi Barang</b></td>
							<td style="text-align:center;width: 1000px;" colspan="5"><b>No. SMU</b></td>
							<td style="text-align:center;" colspan="1"><b>A/C</b></td>
							<td style="text-align:center;width:550px;" colspan="4"><b>Tgl Berangkat</b></td>
							<td style="text-align:center;" colspan="8"><b>Rincian Pengiriman</b></td>
						</tr>
						<!-- ===================================== -->
						<tr>
							<td style="text-align:center;" colspan="4" rowspan="2" contenteditable><?php echo $data_print->isi_barang ?></td>
							<td style="text-align:center;" colspan="5" class="bawah"><?php echo ucwords($data_print->nama_pesawat); ?></td>
							<td style="text-align:center;" colspan="1" rowspan="2"><?php 
								if ($data_print->nama_pesawat=='nam_air') {
									echo "IN";
								}elseif ($data_print->nama_pesawat=='garuda') {
									echo "GA";
								}elseif ($data_print->nama_pesawat=='lion_air') {
									echo "JT";
								}elseif ($data_print->nama_pesawat=='sriwijaya') {
									echo "SJ";
								}elseif ($data_print->nama_pesawat=='citi_link') {
									echo "QG";
								}elseif ($data_print->nama_pesawat=='air_asia') {
									echo "QZ";
								}elseif ($data_print->nama_pesawat=='wing_air') {
									echo "IW";
								}elseif ($data_print->nama_pesawat=='batik_air') {
									echo "ID";
								}elseif ($data_print->nama_pesawat=='truk') {
									echo "TRK";
								}elseif ($data_print->nama_pesawat=='kapal') {
									echo "KPL";
								}
							 ?></td>
							<td style="text-align:center;" colspan="4" rowspan="2"><?php echo date('d M Y', strtotime($data_print->tanggal_berangkat)); ?></td>
							<td colspan="4">&nbsp;Tarif Rp. Per Kg</td>
							<td colspan="4" style="text-align:center;"><b>Jumlah Rp</b></td>
						</tr>
						<!-- ===================================== -->
						<tr>
							<td class="atas" style="text-align:center; font-size:12px;" colspan="5"><?php echo $data_print->no_smu; ?></td>
							<td colspan="2" rowspan="2"><font size="2">&nbsp;<?php 
							echo rupiah($data_print->tarif_per_kg);
						
							 ?></font></td>
							 <td class="text-center" rowspan="2" >X</td>
							<td rowspan="2"><font size="2"><?php echo $data_print->chargeable;?></font></td>
							<td colspan="6" rowspan="2" style="vertical-align:middle;text-align:right;font-size:13px"><?php 
								echo rupiah($data_print->tarif_per_kg*$data_print->chargeable);
								
							 ?></font></td>
						</tr>
						<!-- ===================================== -->
						<tr>
							<td style="text-align:center;" colspan="2"><b>Koli</b></td>
							<td style="text-align:center;" colspan="2"><b>Total Kg/Vol</b></td>
							<td style="text-align:center;" colspan="10"><b>Rincian per Koli</b></td>
						</tr>
						<!-- ===================================== -->
						<tr>
							<td style="text-align:center;" colspan="2" rowspan="2"><?php echo $data_print->koli; ?></td>
							<td class="kanan" style="text-align:center;" colspan="2"><?php echo $data_print->kg; ?></td>
							<td class="kiri">Kg</td>
							<td style="text-align:center;" colspan="9"><?php 
								if ($data_print->type_choice_kg == '0') {
									$vol = explode(',', $data_print->rincian_per_kg);
									for ($i=0; $i <=count($vol)-1; $i++) { 
										if ($vol[$i]==0) {
											
										}else{
											if ($i!=count($vol)-1) {
												echo $vol[$i].",";
											}else{
												echo $vol[$i];
											}
										}
									}
								}else{
									// for ($i=1; $i <= $data_print->jumlah_kali_kg; $i++) { 
									// 	if ($i == $data_print->jumlah_kali_kg) {
											echo $data_print->value_nilai_kg;
										// }else{
										// 	echo $data_print->value_nilai_kg.",";
										// }
									// }
								}
							 ?></td>
							<td colspan="4" rowspan="4" style="width: 350px;"><p style="font-size:10px;"><strong>&nbsp;Biaya-biaya lain: </strong><br>&nbsp; - Service Charge<br>&nbsp; - Karantina <br>&nbsp; - Ppn <br>&nbsp; - Packing <br>&nbsp; - Gudang <br>&nbsp; - Lain-lain <br>&nbsp; - Adm. SMU </p></td>
							<td colspan="4" rowspan="4"><p style="margin-top:0;text-align:right;font-size:10px;">&nbsp;<br><span style="float:left;">:</span><?php echo rupiah($data_print->service_charge); ?><br><span style="float:left;">:</span><?php echo rupiah($data_print->karantina); ?><br><span style="float:left;">:</span><?php echo rupiah($data_print->pick_up); ?><br><span style="float:left;">:</span><?php echo rupiah($data_print->packing); ?><br><span style="float:left;">:</span><?php echo rupiah($data_print->handling); ?><br><span style="float:left;">:</span><?php echo rupiah($data_print->lain_lain); ?><br><span style="float:left;">:</span><?php echo rupiah($data_print->adm_smu); ?></p></td>
						</tr>
						<!-- ===================================== -->
						<tr>
							<td class="kanan" style="text-align:center;" colspan="2"><?php echo $data_print->vol; ?></td>
							<td class="kiri">Vol</td>
							<td style="text-align:center;" colspan="9"><?php 
								if ($data_print->type_choice_vol == '0') {
									$vol = explode(',', $data_print->rincian_per_vol);
									for ($i=0; $i <=count($vol)-1 ; $i++) { 
										if ($vol[$i]==0) {
											
										}else{
											if ($i!=count($vol)-1) {
												echo $vol[$i].",";
											}else{
												echo $vol[$i];
											}
										}
									}
								}else{
									// for ($i=1; $i <= $data_print->jumlah_kali_vol; $i++) { 
									// 	if ($i == $data_print->jumlah_kali_vol) {
											echo $data_print->value_nilai_vol;
									// 	}else{
									// 		echo $data_print->value_nilai_vol.",";
									// 	}
									// }
								}
							 ?></td>
						</tr>
						<!-- ===================================== -->
						<tr>
							<td style="text-align:justify;font-size:9px;" margin-right:50px; colspan="14">Isi barang dinyatakan sesuai dan benar menurut pengakuan pengirim, tidak berupa cairan dan tidak  berbahaya serta aman untuk semua jenis pengangkutan. Dengan ini, pengirim menyatakan dan menyetujui bahwa segala resiko menjadi tanggung jawab pengirim, serta pengirim menyetujui syarat-syarat dan ketentuan pengiriman PT TRANS SARANA JAYA dan pihak pengangkut/airline.</td>
						</tr>
						<!-- ===================================== -->
						<tr>
							<td colspan="7" class="bawah">Cara Pembayaran:</td>
							<td style="text-align:center;" colspan="7" class="bawah">Persetujuan Pengirim</td>
						</tr>
						<!-- ===================================== -->
						<tr>
							<td colspan="3" class="atas kanan"><input type="checkbox" disabled="disabled" <?php if($data_print->cara_pembayaran=='cash'){ echo "checked"; } ?>> CASH</td>
							<td colspan="4" class="atas kiri"><input type="checkbox" disabled="disabled" <?php if($data_print->cara_pembayaran=='kredit'){ echo "checked"; } ?>> CREDIT</td>
							<td colspan="7" style="height: 1cm;" class="atas"></td>
							<td colspan="4"><h4 style="margin:0;text-align:left;font-size: 15px;"><strong>&nbsp;T O T A L</strong></h4></td>
							<?php $biayalain=$data_print->karantina+$data_print->pick_up+$data_print->packing+$data_print->handling+$data_print->lain_lain+$data_print->adm_smu+$data_print->service_charge; ?>
							<td colspan="4"><p style="margin:0;text-align:right;font-size:14px;"><strong><?php 
								echo rupiah(($data_print->tarif_per_kg*$data_print->chargeable)+$biayalain); 
							
							?></strong></p></td>
						</tr>
						<!-- ===================================== -->
					</tbody>
				</table>

				<div class="row text-center" style="padding-top:8px;">
					<div class="col-sm-4 col-xs-4 col-md-4">
						<?php if (!empty($data_print->no_rek) OR $data_print->no_rek!=0): ?>
							<div style="border:3px solid #BDC58C;border-radius: 17px 0px 17px 0px;">
								<b><h6 style="margin-top:3px;font-size:12px;">Rekening <?php echo strtoupper($data_print->bank); ?> a/n Rudy Wiranto</h6></b>
								<b><h6 style="margin-top:-5px;margin-bottom:3px;font-size:12px;"><?php echo $data_print->no_rek ?></h6></b>
							</div>
						<?php endif ?>
					</div>
					<div class="col-sm-4 col-xs-4 col-md-4">
						<a href="http://www.transsaranajaya.com" class="hidden-print" target="blank" style="font-size:12px;"><strong>www.transsaranajaya.com</strong></a>
						<h6 class="visible-print">www.transsaranajaya.com</h6>
					</div>
					<div class="col-sm-4 col-xs-4 col-md-4">
						<p>Yogyakarta, <?php echo date('d F Y', strtotime($data_print->tanggal_dibuat)); ?></p>
						<p>PT. TRANS SARANA JAYA</p>
						<p><?php echo ucwords($data_print->nama_personel) ?> / <?php echo ucwords($data_print->tanggal_dibuat) ?></p>
					</div>
				</div>
			</div>
			<!-- 2 -->
			<div class="container" style="margin-top:1.5cm;">
				<table style="width:100%;" class="table-hover table-bordered">
					<tbody>
						<tr>
							<td width="20"></td>
							<td width="20"></td>
							<td width="20"></td>
							<td width="20"></td>
							<td width="20"></td>
							<td width="20"></td>
							<td width="20"></td>
							<td width="20"></td>
							<td width="20"></td>
							<td width="20"></td>
							<td width="20"></td>
							<td width="20"></td>
							<td width="20"></td>
							<td width="20"></td>
							<td width="20"></td>
							<td width="20"></td>
							<td width="20"></td>
							<td width="20"></td>
							<td width="20"></td>
							<td width="20"></td>
							<td width="20"></td>
							<td width="20"></td>
						</tr>
						<tr>
							<td style="vertical-align:middle;border-right:0 !important;" colspan="2" rowspan="3"> <img class="img-responsive" style="width:100px;" src="<?php echo base_url('image/logo.jpg'); ?>" alt="Logo"> </td>
							<td style="vertical-align:middle;border-left:0 !important;border-bottom:0 !important;" colspan="16"><h4 style="margin:0;"><font face="Baskerville Old Face">PT. TRANS SARANA JAYA</font></h4></td>
							<td style="text-align:center;" colspan="4"><h5 style="margin:0;">INVOICE NO.</h5></td>
						</tr>
						<!-- ===================================== -->
						<tr>
							<td colspan="16" style="border-left:0 !important;border-top:0 !important;border-bottom:0 !important;">JL. NGEKSIGONDO NO.55, KOTAGEDE (DEPAN RS KIA Permata Bunda) YOGYAKARTA</td>
							<td style="font-size:20px;" colspan="4" class="text-center" rowspan="2"><strong><?php echo $data_print->id_penerima; ?></strong></td>
							<!-- <td style="text-align:center; font-size:15px;" colspan="3"></td> -->
						</tr>
						<!-- ===================================== -->
						<tr>
							<td colspan="16" style="border-left:0 !important;border-top:0 !important;">No. CS : <strong>0823 2741 6666 / 0857 2844 4427</strong>  &nbsp;&nbsp;&nbsp;No. Fax : 0274 2840296</td>
						</tr>
						<!-- ===================================== -->
						<tr>
							<td colspan="9" style="border-bottom:0 !important;"><h5 style="margin:0;">PENGIRIM</h5></td>
							<td colspan="9" style="border-bottom:0 !important;"><h5 style="margin:0;">PENERIMA</h5></td>
							<td style="text-align:center;" colspan="2">ASAL</td>
							<td style="text-align:center;" colspan="2"><?php echo ucwords($data_print->asal) ?></td>
						</tr>
						<!-- ===================================== -->
						<tr>
							<td colspan="1" class="bawah" style="border-top:0 !important;border-right:0 !important;">Nama</td>
							<td colspan="8" class="bawah" style="width: 6088px;border-top:0 !important;border-left:0 !important;">: <?php echo ucwords($data_print->nama_pengirim) ?></td>
							<td colspan="1" class="bawah" style="border-top:0 !important;border-right:0 !important;">Nama</td>
							<td colspan="8" class="bawah" style="border-top:0 !important;border-left:0 !important;">: <?php echo ucwords($data_print->nama_penerima) ?></td>
							<td style="text-align:center;" colspan="2">TUJUAN</td>
							<td style="text-align:center;" colspan="2"><b><?php echo strtoupper($data_print->kode_airline) ?></b></td>
						</tr>
						<!-- ===================================== -->
						<tr>
							<td colspan="1" class="atas kanan bawah">Alamat</td>
							<td colspan="8" rowspan="3" valign="top" class="atas kiri bawah">: <?php echo ucwords($data_print->alamat_pengirim) ?></td>
							<td colspan="1" class="atas kanan bawah">Alamat</td>
							<td colspan="8" rowspan="3" valign="top" class="atas kiri bawah">: <?php echo ucwords($data_print->alamat_penerima) ?></td>
							<td style="text-align:center;" colspan="4"><b><?php echo strtoupper($data_print->tujuan) ?></b></td>
						</tr>
						<!-- ===================================== -->
						<tr>
							<td colspan="1" rowspan="2" class="atas kanan bawah"></td>
							<td colspan="1" rowspan="2" class="atas kanan bawah"></td>
							<td style="text-align:center;" colspan="4">Jenis Pengiriman</td>
						</tr>
						<!-- ===================================== -->
						<tr>
							<td style="text-align:center;" colspan="2" rowspan="2"><?php echo strtoupper($data_print->jenis_pengiriman); ?></td>
							<td style="text-align:center;" colspan="2" rowspan="2"><?php echo strtoupper($data_print->service); ?></td>
						</tr>
						<!-- ===================================== -->
						<tr>
							<td colspan="1" class="atas kanan">Telp.</td>
							<td colspan="8" style="padding-top:0px;" class="atas kiri">: <?php echo ucwords($data_print->no_telp_pengirim) ?></td>
							<td colspan="1" class="atas kanan">Telp.</td>
							<td colspan="8" class="atas kiri">: <?php echo ucwords($data_print->no_telp_penerima) ?></td>
						</tr>
						<!-- ===================================== -->
						<tr>
							<td style="text-align:center;" colspan="4"><b>Isi Barang</b></td>
							<td style="text-align:center;width: 1000px;" colspan="5"><b>No. SMU</b></td>
							<td style="text-align:center;" colspan="1"><b>A/C</b></td>
							<td style="text-align:center;width:550px;" colspan="4"><b>Tgl Berangkat</b></td>
							<td style="text-align:center;" colspan="8"><b>Rincian Pengiriman</b></td>
						</tr>
						<!-- ===================================== -->
						<tr>
							<td style="text-align:center;" colspan="4" rowspan="2" contenteditable><?php echo $data_print->isi_barang ?></td>
							<td style="text-align:center;" colspan="5" class="bawah"><?php echo ucwords($data_print->nama_pesawat); ?></td>
							<td style="text-align:center;" colspan="1" rowspan="2"><?php 
								if ($data_print->nama_pesawat=='nam_air') {
									echo "IN";
								}elseif ($data_print->nama_pesawat=='garuda') {
									echo "GA";
								}elseif ($data_print->nama_pesawat=='lion_air') {
									echo "JT";
								}elseif ($data_print->nama_pesawat=='sriwijaya') {
									echo "SJ";
								}elseif ($data_print->nama_pesawat=='citi_link') {
									echo "QG";
								}elseif ($data_print->nama_pesawat=='air_asia') {
									echo "QZ";
								}elseif ($data_print->nama_pesawat=='wing_air') {
									echo "IW";
								}elseif ($data_print->nama_pesawat=='batik_air') {
									echo "ID";
								}elseif ($data_print->nama_pesawat=='truk') {
									echo "TRK";
								}elseif ($data_print->nama_pesawat=='kapal') {
									echo "KPL";
								}
							 ?></td>
							<td style="text-align:center;" colspan="4" rowspan="2"><?php echo date('d M Y', strtotime($data_print->tanggal_berangkat)); ?></td>
							<td colspan="4">&nbsp;Tarif Rp. Per Kg</td>
							<td colspan="4" style="text-align:center;"><b>Jumlah Rp</b></td>
						</tr>
						<!-- ===================================== -->
						<tr>
							<td class="atas" style="text-align:center; font-size:12px;" colspan="5"><?php echo $data_print->no_smu; ?></td>
							<td colspan="2" rowspan="2"><font size="2">&nbsp;<?php 
								echo rupiah($data_print->tarif_per_kg);
							 ?></font></td>
							 <td class="text-center" rowspan="2" >X</td>
							<td rowspan="2"><font size="2"><?php echo $data_print->chargeable;?></font></td>
							<td colspan="6" rowspan="2" style="vertical-align:middle;text-align:right;font-size:13px"><?php 
								echo rupiah($data_print->tarif_per_kg*$data_print->chargeable);
							 ?></font></td>
						</tr>
						<!-- ===================================== -->
						<tr>
							<td style="text-align:center;" colspan="2"><b>Koli</b></td>
							<td style="text-align:center;" colspan="2"><b>Total Kg/Vol</b></td>
							<td style="text-align:center;" colspan="10"><b>Rincian per Koli</b></td>
						</tr>
						<!-- ===================================== -->
						<tr>
							<td style="text-align:center;" colspan="2" rowspan="2"><?php echo $data_print->koli; ?></td>
							<td class="kanan" style="text-align:center;" colspan="2"><?php echo $data_print->kg; ?></td>
							<td class="kiri">Kg</td>
							<td style="text-align:center;" colspan="9">
							<?php 
								if ($data_print->type_choice_kg == '0') {
									$vol = explode(',', $data_print->rincian_per_kg);
									for ($i=0; $i <=count($vol)-1; $i++) { 
										if ($vol[$i]==0) {
											
										}else{
											if ($i!=count($vol)-1) {
												echo $vol[$i].",";
											}else{
												echo $vol[$i];
											}
										}
									}
								}else{
									// for ($i=1; $i <= $data_print->jumlah_kali_kg; $i++) { 
									// 	if ($i == $data_print->jumlah_kali_kg) {
											echo $data_print->value_nilai_kg;
									// 	}else{
									// 		echo $data_print->value_nilai_kg.",";
									// 	}
									// }
								}
							 ?>
							 </td>
							<td colspan="4" rowspan="4" style="width: 350px;"><p style="font-size:10px;"><strong>&nbsp;Biaya-biaya lain: </strong><br>&nbsp; - Service Charge<br>&nbsp; - Karantina <br>&nbsp; - Ppn <br>&nbsp; - Packing <br>&nbsp; - Gudang <br>&nbsp; - Lain-lain <br>&nbsp; - Adm. SMU </p></td>
							<td colspan="4" rowspan="4"><p style="margin-top:0;text-align:right;font-size:10px;">&nbsp;<br><span style="float:left;">:</span><?php echo rupiah($data_print->service_charge); ?><br><span style="float:left;">:</span><?php echo rupiah($data_print->karantina); ?><br><span style="float:left;">:</span><?php echo rupiah($data_print->pick_up); ?><br><span style="float:left;">:</span><?php echo rupiah($data_print->packing); ?><br><span style="float:left;">:</span><?php echo rupiah($data_print->handling); ?><br><span style="float:left;">:</span><?php echo rupiah($data_print->lain_lain); ?><br><span style="float:left;">:</span><?php echo rupiah($data_print->adm_smu); ?></p></td>
						</tr>
						<!-- ===================================== -->
						<tr>
							<td class="kanan" style="text-align:center;" colspan="2"><?php echo $data_print->vol; ?></td>
							<td class="kiri">Vol</td>
							<td style="text-align:center;" colspan="9"><?php 
								if ($data_print->type_choice_vol == '0') {
									$vol = explode(',', $data_print->rincian_per_vol);
									for ($i=0; $i <=count($vol)-1 ; $i++) { 
										if ($vol[$i]==0) {
											
										}else{
											if ($i!=count($vol)-1) {
												echo $vol[$i].",";
											}else{
												echo $vol[$i];
											}
										}
									}
								}else{
									// for ($i=1; $i <= $data_print->jumlah_kali_vol; $i++) { 
									// 	if ($i == $data_print->jumlah_kali_vol) {
											echo $data_print->value_nilai_vol;
									// 	}else{
									// 		echo $data_print->value_nilai_vol.",";
									// 	}
									// }
								}
							 ?></td>
						</tr>
						<!-- ===================================== -->
						<tr>
							<td style="text-align:justify;font-size:9px;" margin-right:50px; colspan="14">Isi barang di nyatakan sesuai dan benar menurut pengakuan pengirim, tidak berupa cairan dan tidak berbahaya serta aman untuk semua jenis pengangkutan. Dengan ini, pengirim menyatakan dan menyetujui bahwa segala resiko menjadi tanggung jawab pengirim, serta pengirim menyetujui syarat-syarat dan ketentuan pengiriman PT TRANS SARANA JAYA dan pihak pengangkut/airline.</td>
						</tr>
						<!-- ===================================== -->
						<tr>
							<td colspan="7" class="bawah">Cara Pembayaran:</td>
							<td style="text-align:center;" colspan="7" class="bawah">Persetujuan Pengirim</td>
						</tr>
						<!-- ===================================== -->
						<tr>
							<td colspan="3" class="atas kanan"><input type="checkbox" disabled="disabled" <?php if($data_print->cara_pembayaran=='cash'){ echo "checked"; } ?>> CASH</td>
							<td colspan="4" class="atas kiri"><input type="checkbox" disabled="disabled" <?php if($data_print->cara_pembayaran=='kredit'){ echo "checked"; } ?>> CREDIT</td>
							<td colspan="7" style="height: 1cm;" class="atas"></td>
							<td colspan="4"><h4 style="margin:0;text-align:left;font-size: 15px;"><strong>&nbsp;T O T A L</strong></h4></td>
							<?php $biayalain=$data_print->karantina+$data_print->pick_up+$data_print->packing+$data_print->handling+$data_print->lain_lain+$data_print->adm_smu+$data_print->service_charge; ?>
							<td colspan="4"><p style="margin:0;text-align:right;font-size:14px;"><strong><?php 
							echo rupiah(($data_print->tarif_per_kg*$data_print->chargeable)+$biayalain); 
							
							?></strong></p></td>
						</tr>
						<!-- ===================================== -->
					</tbody>
				</table>

				<div class="row text-center" style="padding-top:8px;">
					<div class="col-sm-4 col-xs-4 col-md-4">
						<?php if (!empty($data_print->no_rek) OR $data_print->no_rek!=0): ?>
							<div style="border:3px solid #BDC58C;border-radius: 17px 0px 17px 0px;">
								<b><h6 style="margin-top:3px;font-size:12px;">Rekening <?php echo strtoupper($data_print->bank); ?> a/n Rudy Wiranto</h6></b>
								<b><h6 style="margin-top:-5px;margin-bottom:3px;font-size:12px;"><?php echo $data_print->no_rek ?></h6></b>
							</div>
						<?php endif ?>
					</div>
					<div class="col-sm-4 col-xs-4 col-md-4">
						<a href="http://www.transsaranajaya.com" class="hidden-print" target="blank" style="font-size:12px;"><strong>www.transsaranajaya.com</strong></a>
						<h6 class="visible-print">www.transsaranajaya.com</h6>
					</div>
					<div class="col-sm-4 col-xs-4 col-md-4">
						<p>Yogyakarta, <?php echo date('d F Y', strtotime($data_print->tanggal_dibuat)); ?></p>
						<p>PT. TRANS SARANA JAYA</p>
						<p><?php echo ucwords($data_print->nama_personel) ?> / <?php echo ucwords($data_print->tanggal_dibuat) ?></p>
					</div>
				</div>
			</div>
		<?php }
	}else{
		echo "<center><h3>Data Tidak Ditemukan.</h3></center>";
	}
	 ?>
	
	<script src="<?php echo base_url('dist/js/chosen.jquery.js'); ?>" type="text/javascript"></script>
	<script src="<?php echo base_url('dist/js/prism.js'); ?>" type="text/javascript" charset="utf-8"></script>
</body>
<script>
	$(function() {
		window.print();
	})
</script>
</html>
