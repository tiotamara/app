<style>
	td {    width: 60px;}
	body{
		margin-top: -1.3cm;
		margin-bottom: -1.3cm;
		font-size: 10px;
	}
	.table-bordered>thead>tr>td.semua {
		border-bottom:0 !important;
		border-top:0 !important;
		border-left:0 !important;
		border-right:0 !important;
	}
	.table-bordered>thead>tr>td.atas{
		border-top:0 !important;
	}
	.table-bordered>thead>tr>td.bawah{
		border-bottom:0 !important;
	}
	.table-bordered>thead>tr>td.kanan{
		border-right:0 !important;
	}
	.table-bordered>thead>tr>td.kiri{
		border-left:0 !important;
	}
</style>
<style type="text/css">
	@media print {
		.table-bordered>thead>tr>td.semua {
			border-bottom:0 !important;
			border-top:0 !important;
			border-left:0 !important;
			border-right:0 !important;
		}
	}
	@media print {
		.table-bordered>thead>tr>td.atas{
			border-top:0 !important;
		}
	}
	@media print {
		.table-bordered>thead>tr>td.bawah{
			border-bottom:0 !important;
		}
	}
	@media print {
		.table-bordered>thead>tr>td.kanan{
			border-right:0 !important;
		}
	}
	@media print {
		.table-bordered>thead>tr>td.kiri{
			border-left:0 !important;
		}
	}
</style>
<?php 
	if ($_GET['type'] == 'print') { ?>
		<!DOCTYPE html>
		<html lang="en">
		<head>
			<title><?php echo $title; ?></title>
			<link rel="stylesheet" href="<?php echo base_url('dist/css/bootstrap.min.css'); ?>">
			<!-- <link rel='stylesheet prefetch' href='http://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900|RobotoDraft:400,100,300,500,700,900'> -->
			<link rel='stylesheet prefetch' href='<?php echo base_url('dist/css/font-awesome.min.css'); ?>'>
			<script src='<?php echo base_url('dist/js/jquery.min.js'); ?>'></script>
			<!-- ===================== -->
			<style>
				td {    width: 60px;}
			</style>
		</head>
		<body>
			<div class="container">
			<p class="text-right" style="padding-right:65px;"><?php echo $tgl ? 'Tanggal : <b>'.$tgl.'</b>' : ''; ?></p>
			<!-- <h3 class="text-center">PT. TRANS SARANA JAYA</h3> -->
			<h5 class="text-center">Laporan Penjualan Harian</h5>

				<table class="table table-hover table-bordered" style="text-align: left;border:0;">
					<thead>
						<tr>
							<td class="kiri kanan atas" width="120"></td>
							<td class="kiri kanan atas" width="100"></td>
							<td class="kiri kanan atas"></td>
							<td class="kiri kanan atas" width="100"></td>
							<td class="kiri kanan atas"></td>
							<td class="kiri kanan atas"></td>
							<td class="kiri kanan atas"></td>
							<td class="kiri kanan atas"></td>
							<td class="kiri kanan atas"></td>
							<td class="kiri kanan atas"></td>
							<td class="kiri kanan atas"></td>
							<td class="kiri kanan atas" width="100"></td>
							<!-- <td class="kiri kanan atas"></td> -->
							<!-- <td class="kiri kanan atas"></td> -->
						</tr>
						<tr>
							<!-- <th style="vertical-align:middle; text-align:center;" rowspan="2">No</th> -->
							<th style="vertical-align:middle; text-align:center;" rowspan="2">No</th>
							<th style="vertical-align:middle; text-align:center;" rowspan="2">NO. INVOICE</th>
							<th style="vertical-align:middle; text-align:center;" rowspan="2">TUJUAN</th>
							<th style="vertical-align:middle; text-align:center;max-width:110px;" rowspan="2">TONASE<br>/KG</th>
							<th style="vertical-align:middle; text-align:center;max-width:110px;" rowspan="2">TONASE<br>/VOL</th>
							<!-- <th style="vertical-align:middle; text-align:center;" rowspan="2">Biaya Lain</th> -->
							<th style="vertical-align:middle; text-align:center;" rowspan="2">JUMLAH</th>
							<th style="vertical-align:middle; text-align:center;" rowspan="2">CASH</th>
							<th style="vertical-align:middle; text-align:center;" rowspan="2">KREDIT</th>
							<th style="vertical-align:middle; text-align:center;" rowspan="2">REK<br>BANK</th>
							<!-- <th style="vertical-align:middle; text-align:center;" rowspan="2">P/D</th> -->
							<th style="vertical-align:middle; text-align:center;" rowspan="2">A/C</th>
							<th style="vertical-align:middle; text-align:center;" rowspan="2">NO. SMU</th>
							<th style="vertical-align:middle; text-align:center;" colspan="2">KETERANGAN</th>
						</tr>
						<tr>
							<th style="vertical-align:middle; text-align:center;">SHIPPER</th>
							<th style="vertical-align:middle; text-align:center;">KOMODITY</th>
						</tr>
					</thead>
					<tbody>
						<?php 
						$total=array();
						$total_cash=array();
						$tonase=array();
						$tonase_vol=array();
						$total_credit=array();
						if ($laporan->num_rows()>0) {
							$no=1;
							foreach ($laporan->result() as $lap) { ?>
							<tr>
								<td style="text-align:right;"><?php echo $no; ?></td>
								<td style="text-align:center;"><?php echo $lap->id_penerima; ?></td>
								<td style="text-align:center;"><?php echo $lap->tujuan; ?></td>
								<td style="text-align:center;"><?php echo $lap->kg; array_push($tonase, $lap->kg); ?></td>
								<td style="text-align:center;"><?php echo $lap->vol; array_push($tonase_vol, $lap->vol); ?></td>
								<!-- <?php $biayalain=$lap->karantina+$lap->pick_up+$lap->packing+$lap->handling+$lap->lain_lain+$lap->adm_smu+$lap->service_charge; ?> -->
								<!-- <td style="text-align:right;"><?php echo "Rp.".number_format($biayalain,0,',','.'); ?></td> -->
								<td style="text-align:right;"><?php 
									echo number_format($lap->subtotal,0,',','.');
									array_push($total_cash, $lap->subtotal); 
								
								 ?></td>
								<?php if ($lap->cara_pembayaran == 'cash'): ?>
									<td style="text-align:right;"><?php 
									echo number_format($lap->subtotal,0,',','.');
									array_push($total, $lap->subtotal); 
									
									 ?></td>
									<td>-</td>
								<?php else: ?>
									<td style="text-align:right;"><?php echo number_format($lap->dp_kredit,0,',','.'); ?></td>
									<td style="text-align:right;"><?php 
									array_push($total, $lap->dp_kredit); 
									echo number_format($lap->subtotal-$lap->dp_kredit,0,',','.');
									array_push($total_credit, $lap->subtotal-$lap->dp_kredit); 
									
									 ?></td>
								<?php endif ?>
								<td style="text-align:center;"><?php echo $lap->bank; ?></td>
								<!-- <td style="text-align:center;"><?php echo $lap->service; ?></td> -->
								<td style="text-align:center;">
									<?php 
									if ($lap->nama_pesawat=='nam_air') {
										echo "IN";
									}elseif ($lap->nama_pesawat=='garuda') {
										echo "GA";
									}elseif ($lap->nama_pesawat=='lion_air') {
										echo "JT";
									}elseif ($lap->nama_pesawat=='sriwijaya') {
										echo "SJ";
									}elseif ($lap->nama_pesawat=='citi_link') {
										echo "QG";
									}elseif ($lap->nama_pesawat=='air_asia') {
										echo "QZ";
									}elseif ($lap->nama_pesawat=='wing_air') {
										echo "IW";
									}elseif ($lap->nama_pesawat=='batik_air') {
										echo "ID";
									}elseif ($lap->nama_pesawat=='truk') {
										echo "TRK";
									}elseif ($lap->nama_pesawat=='kapal') {
										echo "KPL";
									}
									 ?>
								</td>
								<td style="text-align:right;"><?php echo $lap->no_smu; ?></td>
								<td><?php echo $lap->nama_pengirim; ?></td>
								<td><small><?php echo $lap->isi_barang; ?></small></td>
							</tr>
							<?php	
							$no++;
						}?>
							<tr>
								<td colspan="3" style="text-align:center;"><b>T O T A L</b></td>
								<td style="text-align:center;"><b><?php echo array_sum($tonase); ?></b></td>
								<td style="text-align:center;"><b><?php echo array_sum($tonase_vol); ?></b></td>
								<td style="text-align:center;"><b><?php echo number_format(array_sum($total_cash),0,',','.'); ?></b></td>
								<td style="text-align:center;"><b><?php echo number_format(array_sum($total),0,',','.'); ?></b></td>
								<td style="text-align:center;"><b><?php echo number_format(array_sum($total_credit),0,',','.'); ?></b></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
							</tr>
					<?php
					}else{
						echo "<td colspan='14'>Empty</td>";
					}
					?>
				</tbody>
			</table><div class="clearfix"></div>
			<table>
				<tr>
					<td style="width:300px;">Total Keseluruhan Transaksi </td>
					<td style="width:70px;">= Rp. </td>
					<td style="width:200px;text-align: right;"><?php echo number_format(array_sum($total_cash),0,',','.'); ?></td>
				</tr>
				<tr>
					<td style="width:300px;">Total Transaksi Cash </td>
					<td style="width:70px;">= Rp. </td>
					<td style="width:200px;text-align: right;"><?php echo number_format(array_sum($total),0,',','.'); ?></td>
				</tr>
				<tr>
					<td style="width:300px;">Total Transaksi Credit </td>
					<td style="width:70px;">= Rp. </td>
					<td style="width:200px;text-align: right;"><?php echo number_format(array_sum($total_credit),0,',','.'); ?></td>
				</tr>
			</table>
		</div>
		</body>
		<script>
			$(function() {
				window.print();
			})
		</script>
		</html>
	<?php }elseif($_GET['type'] == 'excel'){
		$file="data.xls";
		header("Content-type: application/vnd.ms-excel");
		header("Content-Disposition: attachment; filename=$file"); ?>
		<html lang="en">
		<head>
			<title><?php echo $title; ?></title>
			<link rel="stylesheet" href="<?php echo base_url('dist/css/bootstrap.min.css'); ?>">
			<!-- <link rel='stylesheet prefetch' href='http://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900|RobotoDraft:400,100,300,500,700,900'> -->
			<link rel='stylesheet prefetch' href='<?php echo base_url('dist/css/font-awesome.min.css'); ?>'>
			<script src='<?php echo base_url('dist/js/jquery.min.js'); ?>'></script>
			<!-- ===================== -->
			<style>
				td {    width: 60px;}
			</style>
		</head>
		<body>
			<div class="container">
			<!-- <p class="text-right"><?php echo $tgl ? 'Tanggal : '.$tgl : ''; ?></p> -->
			<!-- <h3 class="text-center">PT. TRANS SARANA JAYA</h3> -->
			<h5 class="text-center">LAPORAN PENJUALAN HARIAN / L P H <?php echo strtoupper(date('d F Y',strtotime($tgl))); ?></h5>

				<table class="table table-hover table-bordered" style="width:100%;text-align: left;" border="1">
					<thead>
						<!-- <tr>
							<td class="kiri kanan atas"></td>
							<td class="kiri kanan atas"></td>
							<td class="kiri kanan atas"></td>
							<td class="kiri kanan atas"></td>
							<td class="kiri kanan atas"></td>
							<td class="kiri kanan atas"></td>
							<td class="kiri kanan atas"></td>
							<td class="kiri kanan atas"></td>
							<td class="kiri kanan atas"></td>
							<td class="kiri kanan atas"></td>
							<td class="kiri kanan atas"></td>
							<td class="kiri kanan atas" width="100"></td>
							<td class="kiri kanan atas"></td>
							<td class="kiri kanan atas"></td>
						</tr> -->
						<tr>
							<th style="vertical-align:middle; text-align:center;" rowspan="2">NO</th>
							<th style="vertical-align:middle; text-align:center;" rowspan="2">NO. INVOICE</th>
							<th style="vertical-align:middle; text-align:center;" rowspan="2">TUJUAN</th>
							<th style="vertical-align:middle; text-align:center;max-width:70px;" rowspan="2">TONASE<br>/KG</th>
							<th style="vertical-align:middle; text-align:center;max-width:70px;" rowspan="2">TONASE<br>/VOL</th>
							<!-- <th style="vertical-align:middle; text-align:center;" rowspan="2">Biaya Lain</th> -->
							<th style="vertical-align:middle; text-align:center;" rowspan="2">JUMLAH</th>
							<th style="vertical-align:middle; text-align:center;" rowspan="2">CASH</th>
							<th style="vertical-align:middle; text-align:center;" rowspan="2">KREDIT</th>
							<th style="vertical-align:middle; text-align:center;" rowspan="2">REKENING<br>BANK</th>
							<!-- <th style="vertical-align:middle; text-align:center;" rowspan="2">P/D</th> -->
							<th style="vertical-align:middle; text-align:center;" rowspan="2">A/C</th>
							<th style="vertical-align:middle; text-align:center;" rowspan="2">NO. SMU</th>
							<th style="vertical-align:middle; text-align:center;" colspan="2">KETERANGAN</th>
						</tr>
						<tr>
							<th style="vertical-align:middle; text-align:center;">SHIPPER</th>
							<th style="vertical-align:middle; text-align:center;">KOMODITY</th>
						</tr>
					</thead>
					<tbody>
						<?php 
						$total=array();
						$total_cash=array();
						$tonase=array();
						$tonase_vol=array();
						$total_credit=array();
						if ($laporan->num_rows()>0) {
							$no=1;
							foreach ($laporan->result() as $lap) { ?>
							<tr>
								<td style="vertical-align:middle;text-align:right;"><?php echo $no;$no++; ?></td>
								<td style="vertical-align:middle;" width="70"><?php echo $lap->id_penerima; ?></td>
								<td style="vertical-align:middle;"><?php echo $lap->tujuan; ?></td>
								<td style="vertical-align:middle;"><?php echo $lap->kg;array_push($tonase, $lap->kg); ?></td>
								<td style="vertical-align:middle;"><?php echo $lap->vol;array_push($tonase_vol, $lap->vol); ?></td>
								<!-- <?php $biayalain=$lap->karantina+$lap->pick_up+$lap->packing+$lap->handling+$lap->lain_lain+$lap->adm_smu+$lap->service_charge; ?> -->
								<!-- <td style="vertical-align:middle;text-align:right;"><?php //echo "Rp.".number_format($biayalain,0,',','.'); ?></td> -->
								<td style="vertical-align:middle;text-align:right;"><?php 
									echo $lap->subtotal;
									array_push($total_cash, $lap->subtotal); 
								
								 ?></td>
								<?php if ($lap->cara_pembayaran == 'cash'): ?>
									<td style="vertical-align:middle;text-align:right;"><?php 
										echo $lap->subtotal;
										array_push($total, $lap->subtotal); 
									
									 ?></td>
									<td>-</td>
								<?php else: ?>
									<td style="vertical-align:middle;text-align:right;"><?php echo number_format($lap->dp_kredit,0,',','.'); ?></td>
									<td style="vertical-align:middle;text-align:right;"><?php 
										array_push($total, $lap->dp_kredit); 
										echo $lap->subtotal-$lap->dp_kredit;
										array_push($total_credit, $lap->subtotal-$lap->dp_kredit); 
									
									 ?></td>
								<?php endif ?>
								<td style="vertical-align:middle;text-align:center;"><?php echo $lap->bank; ?></td>
								<!-- <td style="vertical-align:middle;text-align:center;"><?php echo $lap->service; ?></td> -->
								<td style="vertical-align:middle;text-align:center;">
									<?php 
									if ($lap->nama_pesawat=='nam_air') {
										echo "IN";
									}elseif ($lap->nama_pesawat=='garuda') {
										echo "GA";
									}elseif ($lap->nama_pesawat=='lion_air') {
										echo "JT";
									}elseif ($lap->nama_pesawat=='sriwijaya') {
										echo "SJ";
									}elseif ($lap->nama_pesawat=='citi_link') {
										echo "QG";
									}elseif ($lap->nama_pesawat=='air_asia') {
										echo "QZ";
									}elseif ($lap->nama_pesawat=='wing_air') {
										echo "IW";
									}elseif ($lap->nama_pesawat=='batik_air') {
										echo "ID";
									}elseif ($lap->nama_pesawat=='truk') {
										echo "TRK";
									}elseif ($lap->nama_pesawat=='kapal') {
										echo "KPL";
									}
									 ?>
								</td>
								<td style="vertical-align:middle;text-align:right;max-width:100px;" width="100"><?php echo $lap->no_smu; ?></td>
								<td style="vertical-align:middle;"><?php echo $lap->nama_pengirim; ?></td>
								<td style="vertical-align:middle;"><small><?php echo $lap->isi_barang; ?></small></td>
							</tr>
							<?php	
							$no++;
						} ?>
							<tr>
								<td colspan="3" style="text-align:center;"><b>T O T A L</b></td>
								<td style="text-align:center;"><b><?php echo array_sum($tonase); ?></b></td>
								<td style="text-align:center;"><b><?php echo array_sum($tonase_vol); ?></b></td>
								<td style="text-align:center;"><b><?php echo array_sum($total_cash); ?></b></td>
								<td style="text-align:center;"><b><?php echo array_sum($total); ?></b></td>
								<td style="text-align:center;"><b><?php echo array_sum($total_credit); ?></b></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
							</tr>
					<?php }else{
						echo "<td colspan='14'>Empty</td>";
					}
					?>
				</tbody>
			</table>
			<br>
			<br>
		<div class='text-right'><?php echo "<span class='text-right'>Total Keseluruhan Transaksi = Rp. ".number_format(array_sum($total_cash),0,',','.')."</span>"; ?><br>
		<?php echo "<span class='text-right'>Total Transaksi Cash = Rp. ".number_format(array_sum($total),0,',','.')."</span>"; ?><br>
		<?php echo "<span class='text-right'>Total Transaksi Credit = Rp. ".number_format(array_sum($total_credit),0,',','.')."</span>"; ?></div>
		</div>
		</body>
		</html>
	<?php }else{
		echo "<center><h2>Terjadi Kesalahan, Silahakan Ulangi lagi.</h2></center>";
	}
 ?>