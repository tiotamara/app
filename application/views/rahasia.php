<!DOCTYPE html>
<html lang="en">
<head>
	<title><?php echo $title; ?></title>
	<link rel="stylesheet" href="<?php echo base_url('dist/css/bootstrap.min.css'); ?>">
	<!-- 	<link rel="stylesheet" href="<?php echo base_url('dist/css/style_prism.css'); ?>"> -->
	<link rel="stylesheet" href="<?php echo base_url('dist/css/chosen.css'); ?>">
	<!-- ================= -->
	<link rel="stylesheet" href="<?php echo base_url('dist/css/custom.css'); ?>">
	<link rel="stylesheet" href="<?php echo base_url('dist/js/jquery/jquery-ui.css'); ?>">
	<!-- <link rel='stylesheet prefetch' href='http://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900|RobotoDraft:400,100,300,500,700,900'> -->
	<link rel='stylesheet prefetch' href='<?php echo base_url('dist/css/font-awesome.min.css'); ?>'>
	<link rel='stylesheet prefetch' href='<?php echo base_url('dist/DataTables/datatables.css'); ?>'>
	<script src='<?php echo base_url('dist/js/jquery.min.js'); ?>'></script>
	<script src='<?php echo base_url('dist/DataTables/datatables.min.js'); ?>'></script>
	<script src='<?php echo base_url('dist/js/jquery/jquery-ui.min.js'); ?>'></script>
	<script src='<?php //echo base_url('dist/js/bootstrap.min.js'); ?>'></script>
	<!-- ===================== -->
	<style>
		.logo:hover{
			box-shadow: 0px 5px 30px -15px #000;
		}
	</style>
</head>
<body>
	<div class="container">
		<div class="row">
			<a href="<?php echo base_url('index/admin'); ?>"><center><img src="<?php echo base_url('image/logo.jpg') ?>" class="img-responsive logo"></center></a>
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<div class="box">
					<div class="box-icon">
						<span class="fa fa-4x fa-history"></span>
					</div>
					<div class="info">
						<h4 class="text-center">History</h4>
						<p>Report data history</p><hr>
							<table class="table table-hover" style="text-align: left;" id="data-table">
								<thead>
									<tr>
										<th>No</th>
										<th>Password</th>
										<th>Edit</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>1</td>
										<td><?php echo $password->secret_password; ?></td>
										<td><button class="btn" type="btn" id="edit"><span class="glyphicon glyphicon-edit"></span></button></td>
									</tr>
								</tbody>
							</table>
							<br>
							<div class="row" id="form-edit" style="display:none;">
								<form method="POST" accept="">
									<div class="col-md-10">
										<input type="text" name="new_password" autocomplete="off" class="form-control" placeholder="New Password" required value="<?php echo $password->secret_password; ?>">
									</div>
									<div class="col-md-2">
										<button class="btn btn-primary btn-block" type="submit">Change</button>
									</div>
								</form>
							</div>
					</div>
				</div>
			</div>
		</div>
		<div class="clearfix"></div><br>
		<div class="col-md-12"><a href="<?php echo base_url('index/logout') ?>" class="btn btn-danger btn-lg btn-block" style="border-radius:0px;"><span class="glyphicon glyphicon-off"></span> Logout</a></div>
	</div>
</div>
<div class="footer"><center style="color:#9C9898;">PT. TRANS SARANA JAYA | 2016</center></div>
</body>
</html>
<script>
	$("#edit").click(function() {
		$("#form-edit").slideToggle('slow');
	})
</script>